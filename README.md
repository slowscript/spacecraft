# SpaceCraft

A game similar to Minecraft except with spherical planets instead of an infinite
plane. It was originally just an experiment but I ended up adding
features to make it more like a game. The player can fly, dig into the planets,
build voxel structures on top of the planets, build and fly spacecraft, etc.

![screenshot](screenshot.png)

The project was abandoned in 2020, although I'm thinking about getting back to
it at some point. Let me know if you are interested in the concept and I might
invest more time into it.

Built using the Unity Engine. If you want to try it out, install
the Unity editor, load the project and hit Play.
