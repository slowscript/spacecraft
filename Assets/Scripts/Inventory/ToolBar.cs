﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[RequireComponent(typeof(HorizontalLayoutGroup))]
public class ToolBar : InventoryUI
{
    public RectTransform highlight;

    public int selectedToolId = 0;
    public Tool selectedTool { get {
            if (inventory.items[selectedToolId,0] != null)
                return inventory.items[selectedToolId,0].GetTool();
            else
                return hand;
    }}

    const float itemSize = 50;

    private KeyCode[] alphaKeyCodes = {
        KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5,
        KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9, KeyCode.Alpha0
    };

    private Hand hand = new Hand();
	
    public void Reset()
    {
        sizeX = 10;
        sizeY = 1;
    }

	// Update is called once per frame
	void Update ()
    {
        int newScrollPos = selectedToolId;

        /**Update toolbar based on keys **/
        for (int i = 0; i < alphaKeyCodes.Length; i++)
        {
            if (Input.GetKeyDown(alphaKeyCodes[i]))
                newScrollPos = i;
        }

        /** Update toolbar based on scrolling **/
        int scroll = (int)(Input.mouseScrollDelta.y * 2f);
        newScrollPos += scroll;
        
        if (newScrollPos < 0) newScrollPos = 0;
        else if (newScrollPos >= sizeX) newScrollPos = sizeX - 1;
          
        //If tool changed
        if (newScrollPos != selectedToolId)
        {
            //Switch tools
            if(selectedTool != null) selectedTool.Deactivate();
            selectedToolId = newScrollPos;
            if (selectedTool != null) selectedTool.Activate();
            //Update highlight
            float newX = -itemSize * sizeX / 2 + itemSize / 2 + (itemSize * selectedToolId);
            highlight.localPosition = new Vector3(newX, highlight.localPosition.y, highlight.localPosition.z);
        }

        //Call update on selected tool
        if (selectedTool != null)
           selectedTool.Update();
	}

    public bool AddTool(Item item)
    {
        var result = inventory.AddItem(item);
        UpdateUI();
        return result;
    }
}


