﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class responsible for displaying inventory
/// </summary>
//[RequireComponent(typeof(GridLayoutGroup))]
public class InventoryUI : MonoBehaviour
{
    public int sizeX = 10;
    public int sizeY = 3;
    public GameObject slotPrefab;
    public bool showOnStart;
    public Inventory inventory;
    protected Slot[,] slots;

    void Awake ()
    {
        inventory = new Inventory(sizeX, sizeY);
        slots = new Slot[sizeX, sizeY];
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                slots[x, y] = Instantiate(slotPrefab, transform).GetComponent<Slot>();
            }
        }

        if (showOnStart)
            Show();
        else
            gameObject.SetActive(false);
	}
	
	public void Show()
    {
        gameObject.SetActive(true);
        UpdateUI();
	}

    public void Hide()
    {
        gameObject.SetActive(false);
        //Put dragged item back to inventory
        if (Inventory.draggedItem != null)
        {
            //Ensures dragged item will not be lost and can be handled by other inventory
            if (inventory.AddItem(Inventory.draggedItem))
                Inventory.EndDrag();
        }
    }

    public void UpdateUI()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                slots[x, y].SetItem(inventory.items[x, y]);
            }
        }
    }

    public void UpdateInventory()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                inventory.items[x, y] = slots[x, y].item;
            }
        }
    }
}
