﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Handles displaying of a stack of items
/// </summary>
public class Slot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Image image;
    public Text label;
    public Item item;
    InventoryUI inventory;
    bool dragged = false;
    public bool acceptsDrop = true;
    public bool sourceSlot = false;
    public delegate bool onGrab();
    public onGrab grabItem;
    public Vector3 tooltipOffset = new Vector3(0, -20, 0);
    IEnumerator tooltipCoroutine;
    GameObject tooltip;

    void Start()
    {
        inventory = transform.parent.GetComponent<InventoryUI>();
    }

    void Update()
    {
        if (dragged)
        {
            transform.position = Input.mousePosition + new Vector3(30f, -30f);
        }
    }

    public void SetDragged()
    {
        dragged = true;
        GetComponent<Button>().interactable = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        CloseTooltip();
        if (eventData.button == PointerEventData.InputButton.Left)
            LeftClick();
        else if (eventData.button == PointerEventData.InputButton.Right)
            RightClick();
        //In case not used within inventory - e.g. in crafting view
        if(inventory != null)
            inventory.UpdateInventory();
    }

    public void OnPointerEnter(PointerEventData data)
    {
        tooltipCoroutine = ShowTooltip();
        StartCoroutine(tooltipCoroutine);
    }

    public void OnPointerExit(PointerEventData data) => CloseTooltip();

    void LeftClick()
    {
        if (sourceSlot)
        {
            //Ensure dragItem is null or matches with item so we can safely grab it
            if (Inventory.draggedItem != null && (
                Inventory.draggedItem.id != item.id ||
                Inventory.draggedItem.value != item.value ||
                Inventory.draggedItem.stackCount >= item.maxStack))
            {
                return;
            }
            if (grabItem())
            {
                if (Inventory.draggedItem == null)
                {
                    Inventory.BeginDrag(item.Clone(), transform.parent);
                }
                else
                {
                    Inventory.draggedItem.stackCount++;
                    Inventory.draggedObject.UpdateUI();
                }
            }
            else
            {
                Player.local.hud.DisplayMessage("Not enough materials");
            }
            return;
        }

        if (item == null)
        {
            if (Inventory.draggedItem != null && acceptsDrop)
            {
                Debug.Log("Drag Drop");
                SetItem(Inventory.draggedItem);
                Inventory.EndDrag();
            }
        }
        else
        {
            if (Inventory.draggedItem != null && acceptsDrop)
            {
                if (item.id == Inventory.draggedItem.id)
                { //Transfer as much of dragged item to this item
                    Debug.Log("Drag Transfer");
                    if (item.stackCount + Inventory.draggedItem.stackCount > item.maxStack)
                    {
                        Inventory.draggedItem.stackCount -= (item.maxStack - item.stackCount);
                        Inventory.draggedObject.UpdateUI();
                        item.stackCount = item.maxStack;
                    }
                    else
                    {
                        item.stackCount += Inventory.draggedItem.stackCount;
                        Inventory.EndDrag();
                    }
                    UpdateUI();
                }
                else
                { //Swap item with dragged item
                    Debug.Log("Drag Swap");
                    Inventory.draggedObject.SetItem(item); // This makes us a value copy (hopefully)
                    SetItem(Inventory.draggedItem);
                    Inventory.draggedItem = Inventory.draggedObject.item;
                }
            }
            else
            {
                Inventory.BeginDrag(item, transform.parent);
                RemoveItem();
            }
        }
    }

    void RightClick()
    {
        if (Inventory.draggedItem != null && acceptsDrop)
        {
            if (item == null)
            {
                item = Inventory.draggedItem.Clone();
                item.stackCount = 1;
                UpdateUI();
                Inventory.draggedItem.stackCount--;
                Inventory.draggedObject.UpdateUI();
            }
            else if (item.id == Inventory.draggedItem.id && (item.stackCount < item.maxStack))
            {
                Inventory.draggedItem.stackCount--;
                Inventory.draggedObject.UpdateUI();
                item.stackCount++;
                UpdateUI();
            }
            if (Inventory.draggedItem.stackCount < 1) //Disable for cheating
                Inventory.EndDrag();
        }
        else if (item != null && (item.stackCount > 1))
        {
            var half = item.Clone();
            half.stackCount = (int)Mathf.FloorToInt(item.stackCount / 2f);
            item.stackCount = (int)Mathf.CeilToInt(item.stackCount / 2f);
            Inventory.BeginDrag(half, transform.parent);
            UpdateUI();
        }
    }

    public void UpdateUI()
    {
        if (item != null)
        {
            image.sprite = item.GetSprite();
            image.color = Color.white;
            label.text = item.stackCount <= 1 ? "" : item.stackCount.ToString();
        }
        else
            RemoveItem();
    }

    public void SetItem(Item _item)
    {
        if (_item != null)
        {
            item = _item;
            UpdateUI();
        }
        else
            RemoveItem();
    }

    public void RemoveItem()
    {
        item = null;
        image.sprite = null;
        image.color = Color.clear;
        label.text = "";
    }

    IEnumerator ShowTooltip()
    {
        yield return new WaitForSeconds(1f);
        if (item == null)
            yield break;
        tooltip = Instantiate(ResourceBank.resources.tooltip, transform.position + tooltipOffset, transform.rotation, transform.root);
        tooltip.GetComponentInChildren<Text>().text = item.GetName() + " : " + item.id;
    }

    void CloseTooltip()
    {
        StopCoroutine(tooltipCoroutine);
        if(tooltip != null)
        {
            Destroy(tooltip);
            tooltip = null;
        }
    }

}
