﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class that hold content of the inventory and some static inventory methods
/// </summary>
public class Inventory
{
    public static Item draggedItem;
    public static Slot draggedObject;
    
    public readonly int sizeX = 10;
    public readonly int sizeY = 3;
    public Item[,] items;

    public Inventory(int x, int y)
    {
        sizeX = x;
        sizeY = y;
        items = new Item[sizeX,sizeY];
    }

    public bool AddItem(Item item)
    {
        if (item.stackCount > item.maxStack)
            throw new UnityException("StackCount is greater than MaxStack. This results into item loss");
        //Find a slot with items of the same type and put as much of the stack into it
        foreach (var i in items)
        {
            if (i == null)
                continue;

            if (i.id == item.id && (i.value == item.value))
            {
                if (i.stackCount < i.maxStack)
                {
                    i.stackCount += item.stackCount;
                    item.stackCount = 0;
                    if (i.stackCount > i.maxStack)
                    {
                        item.stackCount = i.stackCount - i.maxStack;
                        i.stackCount = i.maxStack;
                    }
                }
            }
        }
        //Store the rest in an empty slot
        if (item.stackCount > 0)
        {
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {

                    if (items[x,y] == null)
                    {
                        items[x,y] = item;
                        return true;
                    }
                }
            }
            //No room in inventory
            return false;
        }
        return true;
    }

    public bool RemoveItem(Item item)
    {
        var itm = item.Clone(); //Don't mess with callers version of Item
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (items[x, y] == null)
                    continue;
                if (itm.id == items[x, y].id && (itm.value == items[x,y].value))
                {
                    if (itm.stackCount >= items[x, y].stackCount)
                    {
                        itm.stackCount -= items[x, y].stackCount;
                        items[x, y].Dispose();
                        items[x, y] = null;
                        if (itm.stackCount == 0)
                            return true;
                    }
                    else
                    {
                        items[x, y].stackCount -= itm.stackCount;
                        return true;
                    }
                }
            }
        }
        //Could not find enough items in inventory
        return false;
    }

    public int CountItem(Item item)
    {
        int result = 0;
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (items[x, y] == null)
                    continue;
                if (item.id == items[x, y].id && (item.value == items[x, y].value))
                {
                    result += items[x, y].stackCount;
                }
            }
        }
        return result;
    }

    public Item GetFirst()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (items[x, y] != null)
                {
                    return items[x, y];
                }
            }
        }
        return null;
    }

    public Item RemoveFirst()
    {
        var first = GetFirst();
        var clone = first.Clone();
        clone.stackCount = 1;

        first.stackCount--;
        if (first.stackCount < 1)
            first = null;

        return clone;
    }

    public void ModifyStack(Item itm, int num)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (itm == items[x, y])
                {
                    items[x, y].stackCount += num;
                    if (items[x, y].stackCount == 0)
                    {
                        items[x, y].Dispose();
                        items[x, y] = null;
                    }
                    else if (items[x, y].stackCount < 0)
                        throw new UnityException("The player just got himself into debts. This should not happen");
                    else if (items[x, y].stackCount > items[x, y].maxStack)
                    {
                        var newItm = items[x, y].Clone();
                        newItm.stackCount = items[x, y].stackCount - items[x, y].maxStack;
                        items[x, y].stackCount = items[x, y].maxStack;
                        AddItem(newItm);
                    }
                    return;
                }
            }
        }
    }

    public bool IsEmpty()
    {
        return items.Cast<Item>().All(i => i == null);
    }

    public void Clear()
    {
        System.Array.Clear(items, 0, items.Length);
    }

    public string Serialize()
    {
        string result = "";
        for (int y = 0; y < sizeY; y++)
        {
            for (int x = 0; x < sizeX; x++)
            {               
                if(items[x,y] == null)
                    result += "e;";
                else
                    result += items[x,y].Serialize() + ";";
            }
        }
        return result;
    }

    public void Deserialize(string str)
    {
        var itemStrs = str.Split(';');
        int i = 0;
        for (int y = 0; y < sizeY; y++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                items[x, y] = Item.Deserialize(itemStrs[i]);
                i++;
            }
        }
    }

    public static void BeginDrag(Item item, Transform parent)
    {
        if (draggedItem == null)
        {
            draggedItem = item;
            draggedObject = GameObject.Instantiate(ResourceBank.resources.item, parent).GetComponent<Slot>();
            draggedObject.SetItem(item);
            draggedObject.SetDragged();
            Debug.Log("Drag Begin");
        }
        else
            Debug.LogError("Sorry to tell you, but no drag is happening (dragItem != null)");
    }

    public static void EndDrag()
    {
        draggedItem = null;
        GameObject.Destroy(draggedObject.gameObject);
        Debug.Log("Drag End");
    }
}