﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public GameObject prevGUI;
    public GameObject settingsMenu;
    public Dropdown resolutionsDropdown;
    public Toggle fullScreenToggle;
    public Dropdown qualityDropdown;
    public Slider renderDistanceSlider;
    public Text renderDistanceText;
    public int minRenderDistance = 100;
    public int maxRenderDistance = 1000;

    Resolution[] resolutions;

    void Start()
    {
        resolutions = Screen.resolutions;
        var resList = resolutions.Select((x)=>{return x.width + "x" + x.height;}).ToList();
        resolutionsDropdown.ClearOptions();
        resolutionsDropdown.AddOptions(resList);
        resolutionsDropdown.value = resolutions.ToList().FindIndex((r) => r.height == Screen.height && r.width == Screen.width);
        resolutionsDropdown.RefreshShownValue();

        fullScreenToggle.isOn = Screen.fullScreen;
        
        qualityDropdown.value = QualitySettings.GetQualityLevel();
        qualityDropdown.RefreshShownValue();

        renderDistanceText.text = World.LODSteps.Last().ToString();
        renderDistanceSlider.value = (World.LODSteps.Last() - minRenderDistance) / (float)(maxRenderDistance - minRenderDistance);
    }

    public void ShowSettings(bool show)
    {
        settingsMenu.SetActive(show);
        prevGUI.SetActive(!show);
    }

    public void SetFullscreen(bool value)
    {
        Screen.fullScreen = value;
    }

    public void SetQuality(int idx)
    {
        QualitySettings.SetQualityLevel(idx);
    }

    public void SetResolution(int idx)
    {
        Screen.SetResolution(resolutions[idx].width, resolutions[idx].height, Screen.fullScreen);
    }

    public void SetRenderDistance(float val)
    {
        //val is in range <0;1>
        var dist = (maxRenderDistance - minRenderDistance) * val + minRenderDistance;
        renderDistanceText.text = ((int)dist).ToString();
        World.LODSteps = new int[] {(int)dist*3/10,(int)dist/2 , (int)dist};
    }
}
