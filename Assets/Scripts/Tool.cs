﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class Tool
{
    public const float maxActivateDst = 5f;
    public string name;
    public bool limitClickRate = true;

    public abstract void LeftClick();
    public abstract void Activate();
    public abstract void Update();
    public abstract void Deactivate();
}

//For null or empty
public class Hand : DiggingTool
{
    public Hand(int id = 0) : base(id)
    {
        this.name = "Hand";
        this.digTime = 5f;
        this.voxelWhitelist =  new byte[] {2, 3, 9, 10, 11}; //Grass, dirt, sand, wood, leaves
    }
}

public class DiggingTool : Tool
{
    protected byte[] voxelWhitelist = null;
    bool digging;
    Vector3 currentVoxel;
    float timeStartedDigging;
    float timeSpentDigging;

    //Will be different based on a tool
    protected float digTime = 1f;

	public DiggingTool(int id)
	{
		this.name = "Digging Tool";
        this.limitClickRate = false;
        if(ResourceBank.toolSpeed.ContainsKey(id))
            digTime = ResourceBank.toolSpeed[id];
	}
	public override void LeftClick()
	{
        var cam = Player.local.cam;
		RaycastHit hit;
		if (Physics.Raycast (cam.position, cam.forward, out hit, maxActivateDst))
		{
			if (hit.collider.tag == "Voxel")
			{
                var chunk = hit.collider.GetComponent<PlanetChunk>();
                if (!digging)
                {
                    //Only start digging on whitelisted voxels
                    if(voxelWhitelist != null)
                    {
                        byte id = chunk.GetBlockAtHit(hit);
                        if (!voxelWhitelist.Contains(id))
                            return;
                    }
                    digging = true;
                    currentVoxel = chunk.GetCoordsAtHit(hit);
                    timeStartedDigging = Time.time;
                    timeSpentDigging = 0f;
                }
                else
                {
                    if(chunk.GetCoordsAtHit(hit) != currentVoxel)
                    {
                        StopDigging();
                        return;
                    }

                    timeSpentDigging += Time.deltaTime;
                    
                    //Update UI
                    Player.local.hud.radialProgress.fillAmount = timeSpentDigging / digTime;

                    if(timeSpentDigging >= digTime)
                    {
                        int id = chunk.DestroyAtHit(hit);
                        if (id < 1)
                            return;
                        DigItem(id); //Add to inventory
                        StopDigging();
                    }
                }
			}
		}
	}
    void StopDigging()
    {
        digging = false;
        Player.local.hud.radialProgress.fillAmount = 0f;
    }
    public override void Activate() { }
    public override void Update()
    {
        //Not realiable at very low framerates but acceptable for now
        if(digging && (timeStartedDigging + timeSpentDigging + 0.5f < Time.time))
            StopDigging();
    }
    public override void Deactivate()
    {
        StopDigging();
    }

    public static void DigItem(int id)
    {
        Item itm = ResourceBank.GetItem(9);
        itm.value = id;
        Player.local.PickupItem(itm);
        var extras = ResourceBank.resources.GetExtraDropItems(id);
        foreach (var i in extras)
        {
            float rnd = UnityEngine.Random.Range(0f, 1f);
            if (rnd > i.rarity)
            {
                int count = UnityEngine.Random.Range(i.minCount, i.maxCount+1);
                Item itm2 = ResourceBank.GetItem(i.itemId);
                itm2.stackCount = count;
                Player.local.PickupItem(itm2);
            }
        }
    }
}

public class DestroyTool : Tool
{    
    public DestroyTool()
    {
        this.name = "Destroy Tool";
    }
    public override void LeftClick()
    {
        var cam = Player.local.cam;
        RaycastHit hit;
        if (Physics.Raycast(cam.position, cam.forward, out hit, maxActivateDst))
        {
            if (hit.collider.tag == "Buildable")
            {
                //Prevent destrying something the player is mounted to
                //FIXME: This is just a temporary solution to a bug that annoyed me a lot
                //  Should be checking for every player in server code (NetworkHelper or sth...)
                if (Player.local.transform.IsChildOf(hit.collider.transform))
                    return;
                
                if (hit.transform.childCount < 2)
                    NetworkHelper.current.CmdDestroyObject(hit.transform.gameObject);
                else
                    NetworkHelper.current.CmdDestroyBlock(hit.collider.gameObject);
                int itemID = hit.collider.gameObject.GetComponent<Block>().blockID;
                Player.local.inventory.inventory.AddItem(ResourceBank.GetItem(itemID));
            }
        }
    }
    public override void Activate() { }
    public override void Update() { }
    public override void Deactivate() { }
}

public class PlaceBlockTool : Tool
{
    public const float placeDistance = 5f;
    public int blockID;
    private Item item;
    private GameObject block;
    private MountPoints mounts;
    private float twistAngle = 0f;
    private bool activated = false;
    private Transform cam;
    private GameObject previewBlock;

    public PlaceBlockTool(Item _item)
    {
        item = _item;
        this.blockID = item.id;
        this.name = "Place block " + blockID;
        ResourceBank rb = GameObject.FindObjectOfType<ResourceBank>();
        block = rb.items[blockID].prefab;
    }

    public override void Deactivate()
    {
        if (activated)
        {
            GameObject.Destroy(previewBlock);
            previewBlock = null;
            activated = false;
        }
    }

    public override void Update()
    {
        if (block == null)
            return;
        if (!activated)
            Activate();
        //Cycle mountpoints
        if (Keyboard.current.numpad5Key.wasPressedThisFrame)
            mounts.Next();
        //Select side
        if (Keyboard.current.numpad4Key.wasPressedThisFrame)
            mounts.SetDirection(Sides.Right);
        if (Keyboard.current.numpad6Key.wasPressedThisFrame)
            mounts.SetDirection(Sides.Left);
        if (Keyboard.current.numpad8Key.wasPressedThisFrame)
            mounts.SetDirection(Sides.Bottom);
        if (Keyboard.current.numpad2Key.wasPressedThisFrame)
            mounts.SetDirection(Sides.Top);
        if (Keyboard.current.numpad1Key.wasPressedThisFrame)
            mounts.SetDirection(Sides.Back);
        if (Keyboard.current.numpad3Key.wasPressedThisFrame)
            mounts.SetDirection(Sides.Front);
        //Rotate around normal
        if (Keyboard.current.numpad7Key.wasPressedThisFrame)
            twistAngle -= 90;
        if (Keyboard.current.numpad9Key.wasPressedThisFrame)
            twistAngle += 90;
        RaycastHit hit;
        Quaternion rot = cam.rotation;
        if (Physics.Raycast(cam.position, cam.forward, out hit, maxActivateDst))
        {
            var mp = mounts.GetCurrent(out Vector3 md);
            previewBlock.transform.position = CalculateBlockPos(hit, out rot, block, cam, mp, md, twistAngle);
        }
        else previewBlock.transform.position = cam.position + (cam.forward * placeDistance);
        previewBlock.transform.rotation = rot;
    }

    public override void Activate()
    {
        if (block == null)
            return;
        cam = Player.local.cam;

        if (mounts == null)
        {
            mounts = block.GetComponent<Block>().mounts;
            mounts.Calculate();
        }
        previewBlock = GameObject.Instantiate(block, cam.forward * placeDistance, Quaternion.identity);

        previewBlock.GetComponentsInChildren<Collider>().ToList().ForEach(x => x.enabled = false);
        previewBlock.GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(x => x.enabled = false);
        previewBlock.GetComponentsInChildren<Renderer>().ToList().ForEach(x => {
            x.materials = x.materials.Select(i => ResourceBank.resources.previewMaterial).ToArray();
        });

        activated = true;
    }

    public override void LeftClick()
    {
        if (block == null)
            return;
        var mp = mounts.GetCurrent(out Vector3 md);
        NetworkHelper.current.CmdPlaceBlock(blockID, mp, md, twistAngle);
        Player.local.toolbar.inventory.ModifyStack(item, -1);
        Player.local.toolbar.UpdateUI();
    }

    public static Vector3 CalculateBlockPos(RaycastHit hit, out Quaternion q, GameObject _block, Transform cam, Vector3 selectedMountPoint, Vector3 selectedDir, float twist)
    {
        //TODO: Don't allow placing blocks inside other blocks in one ship, but still allow as many small blocks in a spot as can fit
        //TODO: Check which places on a block can support other blocks
        q = cam.rotation;
        bool hitBuildable = hit.collider.tag == "Buildable";
        if (hitBuildable || hit.collider.tag == "Voxel")
        {
            var pt = hit.transform.InverseTransformPoint(hit.point);
            var ptRnd = hitBuildable ? pt.Round() : pt;
            var locNorm = hit.transform.InverseTransformDirection(hit.normal);
            if(hitBuildable)
                locNorm = locNorm.Round();
            var mountPoint = ptRnd.Mask(locNorm, pt);
            
            //For telling position
            var ql = Quaternion.AngleAxis(twist, locNorm) * Quaternion.FromToRotation(selectedDir, -locNorm);

            //Finally!
            q = hit.transform.rotation * ql;

            var rotatedMountPos = selectedMountPoint.RotateAroundPivot(Vector3.zero, ql.eulerAngles.Round()); //Round to closest int (hopefully multiple of 90)
            var final = mountPoint - rotatedMountPos;
            return hit.transform.TransformPoint(final.SnapToGrid(10f)); //Return final calculated position in global space
        }
        return CalculateBlockPos(cam);
    }

    public static Vector3 CalculateBlockPos(Transform cam)
    {
        return cam.position + (cam.forward * placeDistance);
    }
}

public class VoxelTool : Tool
{
    private Item item;

    public VoxelTool(Item _item)
    {
        item = _item;
        this.name = item.GetName();
    }

    public override void LeftClick()
    {
        RaycastHit hit;
        if (Physics.Raycast(Player.local.cam.position, Player.local.cam.forward, out hit, maxActivateDst))
        {
            if (hit.collider.tag == "Voxel")
            {
                hit.collider.GetComponent<PlanetChunk>().PlaceVoxel(hit, (byte)item.value);
                Player.local.toolbar.inventory.ModifyStack(item, -1);
                Player.local.toolbar.UpdateUI();
            }
        }
    }

    public override void Activate() { }
    public override void Update() { }
    public override void Deactivate() { }
}