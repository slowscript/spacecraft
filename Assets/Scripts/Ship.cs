﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
public class Ship : NetworkBehaviour, ISavable
{
    [SyncVar(hook="OnMassChanged")]
    public float mass = 10f;
    public float hoverModeDrag = 10f;
    internal float throttle = 0.2f;

    private string id;
    private GameObject gm;
    private ResourceBank resources;
    private Rigidbody rb;
    public float[] forceInDirection = new float[6];

    public float energyProduction;
    public float energyConsumption;

    void Awake()
    {
        id = GameManager.GetRandomID(8);
        gm = GameObject.FindWithTag("GM");
        if (gm == null)
            Debug.LogError("No Game Manager found in scene");
        resources = GameObject.FindObjectOfType<ResourceBank>();
        Debug.Assert(resources != null);
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        UpdateShip(isServer);
    }

    void Update()
    {
        //Energy
        var components = GetComponentsInChildren<EnergyComponent>();
        var producers = components.Where((c) => c.type == EnergyComponentType.Producer && c.isTurnedOn);
        var consumers = components.Where((c) => c.type == EnergyComponentType.Consumer && c.isTurnedOn);
        //TODO: Implement batteries
        energyProduction = producers.Sum(p => p.maxOutput);
        energyConsumption = consumers.Sum(c => c.inputNeed);
        if (energyConsumption > energyProduction)
        {
            foreach (var c in consumers)
                c.PowerUpdate(false);  
            
            //TODO: Make things enter a half-powered state - lights flashing, degraded performance etc.
            //TODO: more advanced logic - have some things (lights, backup batteries etc.)
            //      marked as emergency and keep running 
        }
        else
        {
            foreach (var c in consumers)
                c.PowerUpdate(true);
            
            //TODO: Find out if this is physically accurate
            //Distribute load equally among energy sources
            foreach (var p in producers)
            {
                p.currentOutput = energyConsumption / energyProduction * p.maxOutput;
            } 
            //IDEA: Have some sort of priority groups?
        }

    }

    [ClientRpc]
    public void RpcUpdateShip()
    {
        UpdateShip(true);
    }

    public void UpdateShip(bool updateMass)
    {
        foreach(Transform t in transform)
        {
            var b = t.GetComponent<Block>();
            if(b != null)
                b.OnUpdateShip();
        }
        if(updateMass)
        {
            mass = gameObject.GetComponentsInChildren<Block>().Sum(b => b.mass);
            rb.mass = mass;
        }
    }

    public void SetHoverMode(bool value)
    {
        rb.drag = value ? hoverModeDrag : 0f;
    }

    void OnMassChanged(float v)
    {
        rb.mass = v;
    }

    void OnCollisionEnter(Collision col)
    {
        if(!isServer)
            return;
        if(col.relativeVelocity.magnitude < World.minImpactVelocity)
            return;
        col.collider.SendMessage("ProcessCollision", col, SendMessageOptions.DontRequireReceiver);
    }

    public ObjectData SaveObject()
    {
        ObjectData save = new ObjectData();
        save.prefab = "Ship";
        save.transform = new TransformData(transform);
        save.data = new Dictionary<string, string>();
        save.data.Add("id", id);

        var stream = GameManager.GetSaveFileStream(id);
        List<ObjectData> blockdata = new List<ObjectData>();
        foreach (Transform child in transform)
        {
            blockdata.Add(child.GetComponent<Block>().SaveBlock());
        }
        BinaryFormatter bf = new BinaryFormatter();  
        bf.Serialize(stream, blockdata);
        stream.Close();

        return save;
    }

    public void LoadObject(Dictionary<string,string> data)
    {
        foreach (string key in data.Keys)
        {
            switch (key)
            {
                case "id":
                    id = data[key];
                    break;
            }
        }

        var stream = GameManager.GetLoadFileStream(id);
        BinaryFormatter bf = new BinaryFormatter();  
        List<ObjectData> blocks = (List<ObjectData>)bf.Deserialize(stream);
        foreach (var block in blocks)
        {
            var prefab = resources.items[int.Parse(block.prefab)].prefab;
            var newBlock = Instantiate(prefab, block.transform.position, block.transform.rotation, transform);
            newBlock.GetComponent<Block>().LoadBlock(block.data);
            NetworkServer.Spawn(newBlock);
        }
        stream.Close();
    }

    public void RegisterEngine(Vector3 direction, float power)
    {
        forceInDirection[direction.VectorToSide()] += power;
    }

    public void UnregisterEngine(Vector3 direction, float power)
    {
        forceInDirection[direction.VectorToSide()] -= power;
    }
}
