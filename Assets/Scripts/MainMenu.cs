﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

#pragma warning disable CS0618 //Hide all the UNET deprecation warnings
public class MainMenu : MonoBehaviour {

    public NetworkManager nm;
    public GameObject buttonPrefab;
    public GameObject mainUI;
    public GameObject netUI;
    public GameObject newWorldUI;
    public GameObject loadWorldUI;

    public GameObject worldButtons;
    public InputField IP;
    public InputField portConn;
    public InputField portHost;
    public InputField worldName;
    public Text message;

    static string gameFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "SpaceCraft");

    void Start()
    {
        if (!Directory.Exists (gameFolder))
            Directory.CreateDirectory (gameFolder);
    }

    private IEnumerator LoadScene(string name)
    {
		var i = SceneManager.LoadSceneAsync (name);

		while (!i.isDone) {
			Debug.Log ("Loading scene: " + i.progress * 100 + "%");
			yield return null;
		}
	}

    public void NewWorld()
    {
        if (String.IsNullOrEmpty(worldName.text)){
            StartCoroutine(ShowMessage("Please give the world a name"));
            return;
        }

        GameManager.worldName = worldName.text;
        string worldFolder = Path.Combine(gameFolder, worldName.text);
        if (Directory.Exists(worldFolder))
        {
            Debug.LogError("World named " + worldName.text + " already exists!");
            StartCoroutine(ShowMessage("World of this name already exists!"));
            return;
        }
        try{
            Directory.CreateDirectory(worldFolder);
        } catch {
            StartCoroutine(ShowMessage("World cannot be created! Try using a different name."));
            return;
        }

        GameManager.onNetwork = true;
        GameManager.load = false;
        NetworkManager.singleton.networkPort = portHost.text == "" ? 7777 : int.Parse(portHost.text);
        GameManager.saveFolder = worldFolder;
        NetworkManager.singleton.StartHost();
    }

    public void LoadWorld(string _worldName)
    {
        string worldFolder = Path.Combine(gameFolder, _worldName);
        if (!Directory.Exists(worldFolder))
        {
            StartCoroutine(ShowMessage("World not found"));
            return;
        }
        GameManager.onNetwork = true;
        GameManager.load = true;
        NetworkManager.singleton.networkPort = portHost.text == "" ? 7777 : int.Parse(portHost.text);
        GameManager.saveFolder = worldFolder;
        NetworkManager.singleton.StartHost();
    }

	public void Quit()
	{
		Application.Quit ();
	}

    public void ShowNetworkUI()
    {
        mainUI.SetActive(false);
        netUI.SetActive(true);
    }

    public void ShowNewWorldUI()
    {
        mainUI.SetActive(false);
        newWorldUI.SetActive(true);
    }

    public void ShowLoadWorldUI()
    {
        mainUI.SetActive(false);
        loadWorldUI.SetActive(true);
        //Remove old worlds
        var children = new List<GameObject>();
        foreach (Transform child in worldButtons.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
        //Load worlds
        foreach (string worldPath in Directory.GetDirectories(gameFolder))
        {
            string world = Path.GetFileName(worldPath);
            var worldObj = Instantiate(buttonPrefab, worldButtons.transform);
            var button = worldObj.GetComponent<Button>();
            button.onClick.AddListener(()=>{LoadWorld(worldPath);});
            var text = worldObj.GetComponentInChildren<Text>();
            text.text = world;
        }
    }

    public void ShowMainUI()
    {
        netUI.SetActive(false);
        newWorldUI.SetActive(false);
        loadWorldUI.SetActive(false);
        mainUI.SetActive(true);
    }

    public void StartHost(bool load)
    {
        GameManager.onNetwork = true;
        GameManager.load = load;
        NetworkManager.singleton.networkPort = portHost.text == "" ? 7777 : int.Parse(portHost.text);
        NetworkManager.singleton.StartHost();
    }

    public void Connect()
    {
        NetworkManager.singleton.networkAddress = IP.text;
        NetworkManager.singleton.networkPort = portConn.text == "" ? 7777 : int.Parse(portConn.text);
        NetworkManager.singleton.StartClient();
        GameManager.onNetwork = true;
        GameManager.load = false;
        GameManager.saveFolder = Path.Combine(gameFolder, "remote world");
    }

    IEnumerator ShowMessage(string msg)
    {
        message.text = msg;
        yield return new WaitForSeconds(2f);
        message.text = "";
    }
}
