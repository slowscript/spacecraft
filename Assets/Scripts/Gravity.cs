﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Gravity : NetworkBehaviour
{
    public Rigidbody rb;
    public bool isActive = false;
    public float gMax = 9.81f;
    public float falloff = 30f;
    //Only for planets and other non-rigidbody objects

    private PlanetObject planet;

    void Awake()
    {
        if (isActive)
            planet = GetComponent<PlanetObject>();
    }
    void Start()
    {
        if (!isServer)
        {
            this.enabled = false;
            return;
        }
        World.bodiesWithGravity.Add(this);
    }

    void OnDestroy()
    {
        World.bodiesWithGravity.Remove(this);
    }

	// Physics update
	void FixedUpdate () {
        if (!isActive)
            return;
        //This runs only on planets
        foreach(var body in World.bodiesWithGravity)
        {
            if ((body == this) || (body.rb == null))
                continue;
            Vector3 direction = transform.position - body.rb.position;
            direction.Normalize();
            float distance = Vector3.Distance(transform.position, body.rb.position);
            distance -= planet.maxHeight;
            float g = gMax;
            if(distance > 0)
            {
                g = gMax / Mathf.Pow(distance / falloff + 1, 2);
            }
            var force = direction * body.rb.mass * g;
            body.rb.AddForce(force);
        }
	}
}
