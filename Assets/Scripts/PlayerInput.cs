﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A script to handle player controls not related to movement.
/// </summary>
[RequireComponent(typeof(PlayerControler))]
public class PlayerInput : MonoBehaviour
{
    public Player player;
    public GameObject spotlight;
    public const float maxActivateDst = 5f;

    GameObject gameManager;
    PlayerControler controller;

    private bool paused = false;
    private bool inInventory = false;
    private GameObject extraUI;
    private float nextClick = 0f;
    public float clickRate = 0.25f;
    public bool thirdPerson = false;
    public float TPSZoom = 5f;

    // Use this for initialization
    void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GM");
        if (gameManager == null)
            Debug.LogError("Couldn't find GameManager in scene!");
        
        controller = GetComponent<PlayerControler>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!paused && !inInventory && controller.cursorLocked)
        {
            //Mouse buttons
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                //Activate object in front of player
                RaycastHit hit;
                if (Physics.Raycast(player.cam.position, player.cam.forward, out hit, maxActivateDst))
                {
                    var activable = hit.collider.transform.GetComponent<IActivable>();
                    if(activable != null)
                        activable.Activate(player);
                }
            }
            else if (Input.GetKey(KeyCode.Mouse0))
            {
                //Use the tool
                if (player.toolbar.selectedTool != null)
                {
                    if(!player.toolbar.selectedTool.limitClickRate || (nextClick < Time.time))
                    {
                        nextClick = Time.time + clickRate;
                        player.toolbar.selectedTool.LeftClick();
                    }
                }
            }

            //Keyboard
            if (Input.GetButtonDown("Inventory"))
            {
                player.hud.craftingPanel.toolsEnabled = new List<CraftingTool>{CraftingTool.Hand};
                ShowInventory();
            }

            if (Input.GetButtonDown("ThirdPerson"))
            {
                thirdPerson = !thirdPerson;
                player.cam.GetChild(0).localPosition = thirdPerson ? new Vector3(0f, 0f, -TPSZoom) : Vector3.zero;
            }

            if (Input.GetButtonDown("Damping"))
            {
                controller.damping = !controller.damping;
            }

            if (Input.GetButtonDown("Spotlight"))
            {
                spotlight.SetActive(!spotlight.activeSelf);
            }

            if (Input.GetButtonDown("ToggleHUD"))
            {
                player.hud.gameObject.SetActive(!player.hud.gameObject.activeSelf);
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                Debug.Log("Instantiate planet");
                NetworkHelper.current.CmdCreatePlanet();
            }

            if (Input.GetKeyDown(KeyCode.B))
            {
                Debug.Log("Cheating blocks");
                for (int i = 0; i < 16; i++)
                {
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(1));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(3));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(7));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(8));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(10));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(11));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(17));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(18));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(19));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(20));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(21));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(23));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(29));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(30));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(34));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(38));
                    player.inventory.inventory.AddItem(ResourceBank.GetItem(39));
                }
                player.toolbar.AddTool(ResourceBank.GetItem(5));
                player.toolbar.AddTool(ResourceBank.GetItem(2));
            }
        }


        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.F10))
        {
            player.hud.ToggleCheatField();
        }

        if (Input.GetButtonDown("Cancel"))
        {
            if (inInventory)
            {
                HideInventory();
            }
            else SetPause(!paused);     
        }
    }

    public void ShowInventory(bool crafting = true, GameObject panel = null)
    {
        player.inventory.Show();
        if(crafting)
        {
            player.hud.craftingPanel.InitRecipes();
            player.hud.craftingPanel.gameObject.SetActive(true);
        }
        inInventory = true;
        controller.SetCursorLock(false);
        if(panel != null)
        {
            extraUI = panel;
            panel.SetActive(true);
        }
    }

    public void HideInventory()
    {
        player.inventory.Hide();
        player.hud.craftingPanel.gameObject.SetActive(false);
        inInventory = false;
        controller.SetCursorLock(true);
        if (extraUI != null)
        {
            Destroy(extraUI);
            extraUI = null;
        }
    }

    public void SetPause(bool value)
    {
        paused = value;
        controller.SetCursorLock(!paused);
        player.hud.escMenu.SetActive(paused);
        if(!GameManager.onNetwork)
            Time.timeScale = paused ? 0 : 1;
    }
}
