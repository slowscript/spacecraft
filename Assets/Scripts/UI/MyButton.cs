﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MyButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Color textNormal;
    public Color textHover;

    public Text text;

    void OnEnable()
    {
        text.color = textNormal;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.color = textHover;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.color = textNormal;
    }
}
