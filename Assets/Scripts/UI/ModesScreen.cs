﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModesScreen : MonoBehaviour
{
	public void ChangeScreen(int id)
	{
		transform.parent.GetComponent<ScreenSwitcher>().SwitchScreen(id);
	}
}
