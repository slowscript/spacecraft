﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagementScreen : MonoBehaviour
{
    public Transform blockList;
    public Transform propList;
    public GameObject button;
    public GameObject toggle;
    public GameObject slider;

    public void ChangeScreen()
    {
        transform.parent.GetComponent<ScreenSwitcher>().SwitchScreen(0);
    }

    public void ShowBlocks(System.Type t)
    {
        //Remove old buttons
        foreach(Transform child in blockList)
            Destroy(child.gameObject);

        GameObject ship = transform.root.gameObject;
        var blocks = ship.GetComponentsInChildren(t);
        print("block count " + blocks.Length);
        for(int i = 0; i < blocks.Length; i++)
        {
            var props = blocks[i].gameObject.GetComponent<Properties>();
            if (props == null)
                continue;
            
            var btn = Instantiate(button, Vector3.zero, blockList.rotation, blockList);
            btn.GetComponentInChildren<Text>().text = blocks[i].transform.name;
            btn.transform.localPosition = Vector3.zero;
            btn.GetComponent<Button>().onClick.AddListener(() => {ShowProperties(props);});
        }
    }

    public void ShowProperties(Properties props)
    {
        //Remove old buttons
        foreach(Transform child in propList)
            Destroy(child.gameObject);

        var list = props.GetProperties();

        // On/Off switch
        var ec = props.GetComponent<EnergyComponent>();
        if (ec != null)
        {
            var onoff = Instantiate(toggle, propList.position, propList.rotation, propList);
            onoff.GetComponentInChildren<Text>().text = "Turned on";
            onoff.GetComponent<Toggle>().isOn = ec.isTurnedOn;
            onoff.GetComponent<Toggle>().onValueChanged.AddListener((val) =>
            {
                ec.TurnOnOff(val);
            });
        }

        foreach(var prop in list)
        {
            switch(prop.type)
            {
                case PropType.Bool:
                    var tgl = Instantiate(toggle, propList.position, propList.rotation, propList);
                    tgl.GetComponentInChildren<Text>().text = prop.name;
                    tgl.GetComponent<Toggle>().isOn = bool.Parse(prop.value);
                    tgl.GetComponent<Toggle>().onValueChanged.AddListener((val) => {
                        if (!props.SetProperty(prop.name, val.ToString()))
                            Player.local.hud.DisplayMessage("Failed to set property");
                    });
                    break;
                case PropType.Int:
                    var sldr = Instantiate(slider, propList.position, propList.rotation, propList);
                    sldr.GetComponentInChildren<Text>().text = prop.name;
                    sldr.GetComponentInChildren<Slider>().maxValue = prop.maxValue;
                    sldr.GetComponentInChildren<Slider>().value = int.Parse(prop.value);
                    sldr.GetComponentInChildren<Slider>().onValueChanged.AddListener((val) => {
                        if (!props.SetProperty(prop.name, val.ToString()))
                            Player.local.hud.DisplayMessage("Failed to set property");
                    });
                    break;
            }
        }
    }

    public void ShowEngines()
    {
        ShowBlocks(typeof(RocketEngine));
    }

    public void ShowDoors()
    {
        ShowBlocks(typeof(Door));
    }

    public void ShowLights()
    {

    }

    public void ShowAll()
    {
        ShowBlocks(typeof(Properties));
    }
}
