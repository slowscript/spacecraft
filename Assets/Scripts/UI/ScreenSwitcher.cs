﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSwitcher : MonoBehaviour
{
	public int defaultScreen;

	void Start()
	{
		SwitchScreen(defaultScreen);
	}
	public void SwitchScreen(int id)
	{
		if (transform.childCount > 0)
			Destroy(transform.GetChild(0).gameObject);
		Instantiate(ResourceBank.resources.screens[id], transform.position, transform.rotation, transform);
	}
}
