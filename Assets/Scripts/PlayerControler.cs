﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PlayerController is a class used to move a player object with Rigidbody
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerControler : MonoBehaviour {

    public float XSensitivity = 2f;
    public float YSensitivity = 2f;
    public float RollSpeed = 20f;
    public bool clampVerticalRotation = true;
    public float MinimumX = -85f;
    public float MaximumX = 85f;
    public float maxForce = 10f;
    public float groundSpeed = 5f;
    public float jumpSpeed = 5f;
    public float groundedCheckDistance = 0.8f; //Half player height + some tolerance
    public bool damping = true;
    public Player player;
    public bool cursorLocked { get; private set; }

    private Rigidbody body;
    private bool planetMode = false;
    private bool walkMode = false;
    private Transform closestPlanet;
    private bool mounted = false;
    private Transform seat;
    private Ship ship;
    bool freeCamera;
    bool freeCursorTmp;
    float v, h, y;

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
        SetCursorLock(true);
    }
	
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            freeCursorTmp = cursorLocked;
            SetCursorLock(false);
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            SetCursorLock(freeCursorTmp);
        }
        
        if (Cursor.visible)
            return;

        //Trigger planet mode
        if (Input.GetButtonDown("PlanetMode"))
        {
            walkMode = false;
            planetMode = !planetMode;
            if (planetMode)
            {
                closestPlanet = World.FindClosestPlanet(transform.position).transform;
                player.hud.DisplayMessage("Planet mode activated");
            }
            else player.hud.DisplayMessage("Planet mode deactivated");
        }

        //Trigger walk mode
        if (Input.GetButtonDown("WalkMode"))
        {
            planetMode = false;
            walkMode = !walkMode;
            if (walkMode)
                player.hud.DisplayMessage("Walk mode activated");
            else player.hud.DisplayMessage("Walk mode deactivated");
        }

        //Throttle
        if(mounted)
        {
            float scroll = Input.mouseScrollDelta.y * 0.04f;
            ship.throttle = Mathf.Clamp01(ship.throttle + scroll);  
        }

        /****  Get movement input  ****/
        ProcessMovementInput();

        /****  Get rotation input  ****/
        float yRot = Input.GetAxis("Mouse X") * XSensitivity;
        float xRot = Input.GetAxis("Mouse Y") * YSensitivity;
        float roll = -Input.GetAxis("Roll") * RollSpeed * Time.deltaTime;
        freeCamera = Input.GetButton("FreeCamera");

        /**** Rotate body or ship ****/
        if (Input.GetButtonUp("FreeCamera"))
            player.cam.localRotation = Quaternion.identity;
        else if (freeCamera)
            player.cam.localRotation *= Quaternion.Euler(0f, yRot, 0f);
        else
        {
            if (mounted)
            {
                body.transform.RotateAround(body.transform.position, seat.up, yRot);
                body.transform.RotateAround(body.transform.position, seat.forward, roll);
            }
            else
                body.transform.localRotation *= Quaternion.Euler(0f, yRot, roll);
        }
        /****   Tilt camera       ****/
        player.cam.localRotation *= Quaternion.Euler (-xRot, 0f, 0f);

        if(clampVerticalRotation)
            player.cam.localRotation = ClampRotationAroundXAxis(player.cam.localRotation);
    }

	void FixedUpdate()
    {
        if (planetMode)
        {
            /**** Rotate player to stand on the planet ****/
            if (closestPlanet != null)
            {
                Vector3 direction = closestPlanet.position -body.transform.position;
                Quaternion toRotation = Quaternion.FromToRotation(-body.transform.up, direction) * body.transform.rotation;
                body.transform.rotation = Quaternion.Lerp(body.transform.rotation, toRotation, 1f);
            }
        }
        else if (walkMode)
        {
            var generator = World.FindClosestGravityGenerator(transform.position);
            if (generator != null)
            {
                Vector3 direction = generator.transform.up;
                Quaternion toRotation = Quaternion.FromToRotation(body.transform.up, direction) * body.transform.rotation;
                body.transform.rotation = Quaternion.Lerp(body.transform.rotation, toRotation, 4f);
            }
        }
        else
        {
            /* Follow camera when going forward */
            if ((v > 0) && !freeCamera)
            {
                if (mounted)
                {
                    var targetRot = player.cam.rotation * Quaternion.Inverse(seat.localRotation);
                    body.rotation = Quaternion.Lerp(body.rotation, targetRot, 0.1f);
                }
                else
                    body.rotation = Quaternion.Lerp(body.rotation, player.cam.rotation, 0.1f);
                player.cam.localRotation = Quaternion.Lerp(player.cam.localRotation, Quaternion.identity, 0.1f);
            }  
        }

        /*if ((planetMode || walkMode) && !mounted)
            MoveWithVelocity();
        else*/ 
        MoveWithForce();  
    }

    void MoveWithForce()
    {
        if (planetMode || walkMode)
        {
            var locVel = transform.InverseTransformVector(body.velocity);
            bool grounded = Physics.Raycast(transform.position, -transform.up, groundedCheckDistance);
            if(grounded && (Mathf.Abs(locVel.y) < 0.1f)) //Don't jump while in air or immediately before landing
            {
                if (y > 0f) //Jump
                    body.AddRelativeForce(0f, jumpSpeed, 0f, ForceMode.VelocityChange);
                else if (y < 0f)
                {}//Crouch
            }
            y = 0f; //Don't handle y input twice

            if(Mathf.Abs(locVel.x) > groundSpeed)
                h = 0f;
            if(Mathf.Abs(locVel.z) > groundSpeed)
                v = 0f;
        }   
        
        Vector3 force = new Vector3(h, y, v);

        //Stop moving when speed is very small and player doesn't want to accelerate
        if ((force.magnitude == 0) && (body.velocity.magnitude < 0.05))
            body.velocity = Vector3.zero;
        else
        {
            if (damping)
            {
                Damping(ref force);
            }
            //Apply final force
            AddRelativeForce(force);
        }
    }

    void MoveWithVelocity()
    {
        bool grounded = Physics.Raycast(transform.position, -transform.up, groundedCheckDistance);

        float jump = body.transform.InverseTransformVector(body.velocity).y;
        if(grounded)
        {
            if (y > 0) //Jump
                jump = jumpSpeed; 
            else if (y < 0)
            {}//Crouch
        }

        var velocityVect = new Vector3(h * groundSpeed, jump, v * groundSpeed);
        body.velocity = body.transform.TransformVector(velocityVect);
    }

    void ProcessMovementInput()
    {
        v = Input.GetAxis("Vertical");
        if(v >= 0) v *= MaxForce(Sides.Front);
        else v *= MaxForce(Sides.Back);
        
        h = Input.GetAxis("Horizontal");
        if(h >= 0)  h *= MaxForce(Sides.Right);
        else h *= MaxForce(Sides.Left);
        
        y = Input.GetAxis("UpDown");
        if(y >= 0) y *= MaxForce(Sides.Top);
        else y *= MaxForce(Sides.Bottom);
    }

    void Damping (ref Vector3 force)
    {
        var locVel = (mounted ? seat : transform).InverseTransformVector(body.velocity);
        var coef = body.mass / Time.fixedDeltaTime;
        if (force.x == 0)
            force.x = Mathf.Clamp(-(locVel.x * coef), -MaxForce(Sides.Left), MaxForce(Sides.Right));
        if (force.y == 0 && !planetMode)
            force.y = Mathf.Clamp(-(locVel.y * coef), -MaxForce(Sides.Bottom), MaxForce(Sides.Top));
        if (force.z == 0)
            force.z = Mathf.Clamp(-(locVel.z * coef), -MaxForce(Sides.Back), MaxForce(Sides.Front));

    }

    float MaxForce(Sides side)
    {
        if (mounted)
            return ship.forceInDirection[(int)side] * ship.throttle;
        else
            return maxForce;
    }

    void AddRelativeForce(Vector3 f)
    {
        if (mounted)
        {
            var newF = (seat.right * f.x) + (seat.up * f.y) + (seat.forward * f.z);
            body.AddForce(newF);
        }
        else
        {
            body.AddRelativeForce(f);
        }
    }

    public void MountShip(Transform _seat, Vector3 pos)
    {
        //Turn off walk/planet mode
        planetMode = false;
        walkMode = false;

        seat = _seat;
        ship = seat.root.GetComponent<Ship>();
        body.isKinematic = true;
        //Get rigidbody of the ship (hierarchy root)
        body = seat.root.GetComponent<Rigidbody>();
        //TODO: Avoid this with rotation damping
        body.freezeRotation = true;
        //Position player into the seat
        transform.position = pos;
        transform.rotation = seat.rotation;
        transform.SetParent(seat);
        mounted = true;
    }

    public void DismountShip()
    {
        if (!mounted)
            return;
        body.freezeRotation = false;
        body = GetComponent<Rigidbody>();
        body.isKinematic = false;
        transform.SetParent(null);
        mounted = false;
        seat = null;
        ship = null;
    }

    public void SetCursorLock(bool value)
    {
        cursorLocked = value;
        UpdateCursorLock();
    }

    private void UpdateCursorLock()
    {
        Cursor.visible = !cursorLocked;
        Cursor.lockState = cursorLocked ? CursorLockMode.Locked : CursorLockMode.None;
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);

        angleX = Mathf.Clamp (angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}
