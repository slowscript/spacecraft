﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Text speedText;
    public Text messageText;
    public Slider healthBar;
    public GameObject escMenu;
    public InputField cheatField;
    public CraftingPanel craftingPanel;
    public InventoryUI inventory;
    public Image radialProgress;

    float nextUpdateTime = 0f;
    float updateRate = 0.5f;

    void Update ()
    {
        if (Time.time > nextUpdateTime)
        {
            speedText.text = "Velocity: " + Player.local.transform.root.GetComponent<Rigidbody>().velocity.magnitude.ToString("F1") + " u/s";
            nextUpdateTime = Time.time + updateRate;

            healthBar.value = Player.local.Health;
        }
    }

    public void DisplayMessage(string msg)
    {
        Debug.Log("[HUD] message: " + msg);
        StartCoroutine(ShowMsg(msg));
    }

    IEnumerator ShowMsg(string msg)
    {
        messageText.text = msg;
        yield return new WaitForSeconds(3f);
        messageText.text = "";
    }

    //Used for Back button in EscMenu
    public void SetPause(bool value)
    {
        Player.local.input.SetPause(value);
    }

    public void ToggleCheatField()
    {
        cheatField.gameObject.SetActive(!cheatField.gameObject.activeSelf);
    }

    public void DevCode(string s)
    {
        var args = s.Split(' ');
        if (args[0] == "cheat")
        {
            Player.local.PickupItem(Item.Deserialize(args[1]));
        }
        else if (s == "inv clear")
        {
            Player.local.inventory.inventory.Clear();
            Player.local.toolbar.inventory.Clear();
            Player.local.toolbar.UpdateUI();
        }
        //cheatField.text = "";
    }
}
