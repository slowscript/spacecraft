﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour, ISavable
{
    //In minutes
    public float dayLength = 30f;


    // Update is called once per frame
    void Update()
    {
        float y = 360f / (dayLength * 60f) * Time.deltaTime; 
        transform.Rotate(0f, y, 0f);
    }

    public ObjectData SaveObject()
    {
        ObjectData save = new ObjectData();
        save.prefab = "Sun";
        save.transform = new TransformData(transform);
        save.data = null;
        return save;
    }

    public void LoadObject(Dictionary<string,string> data)
    {
        //Nothing to load - rotation is stored in transform data
    }
}
