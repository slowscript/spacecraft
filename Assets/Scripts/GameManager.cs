﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

#pragma warning disable CS0618 //Hide all the UNET deprecation warnings
public class GameManager : NetworkBehaviour {
    
    //Set in main scene - determines whether to load a world or begin generating a new one
    public static bool load = false;
    public static bool onNetwork = false;
    public static string worldName;
    public static string saveFolder;
    public static int seed = -1;
    public GameObject playerPrefab;
    public static int connToServer;
    
    public static Vector3 playerPos;
    public static Quaternion playerRot;
    public static Dictionary<string, string> playerData;

    static readonly System.Random rnd = new System.Random();

    void Start()
    {
        if (!Directory.Exists(saveFolder))
            Directory.CreateDirectory(saveFolder);

        if (isServer)
        {
            if (load)
            {
                //Load saved world
                LoadGame();
            }
            else
            {
                //Generate new world
                Debug.Log("[GM] Generating new world");
                
                var builder = GetComponent<WorldBuilder>();
                //Select random seed if the user didn't
                if (seed == -1)
                    seed = rnd.Next();
                builder.seed = seed;
                builder.Build();
                
                if (!onNetwork) //The non-networked player
                    Instantiate(playerPrefab);
                
                Debug.Log("[GM] World generated. Doing first save...");
                SaveGame();
            }
        }
        else
        {
            //We receive all the important data from network. No loading.
            NetworkManager.singleton.client.RegisterHandler(ChunkDataMsg.id, OnChunkDataReceived);
        }
    }

    public void Quit()
    {
        Debug.Log("[GM] Quitting game..");
        if (isServer)
            NetworkManager.singleton.StopHost();
        else
            NetworkManager.singleton.StopClient();
        
        Application.Quit();
    }

    void OnChunkDataReceived(NetworkMessage netMsg)
    {
        ChunkDataMsg msg = netMsg.ReadMessage<ChunkDataMsg>();
        var chunk = World.FindChunkWithID(msg.target);
        chunk.OnDataReceive(msg.GetData());
    }

    public static int GetConnectionToServer()
    {
        if (connToServer == 0)
        {
            var players = GameObject.FindGameObjectsWithTag("Player");
            foreach(var p in players)
            {
                var id = p.GetComponent<NetworkIdentity>();
                if (id.isLocalPlayer)
                    connToServer = id.connectionToServer.connectionId;
            }
        }
        return connToServer;
    }

    private void LoadGame()
    {
        Debug.Log("[GM] Starting game with saved world...");

        var stream = GetLoadFileStream("save");

        BinaryFormatter bf = new BinaryFormatter();
        List<ObjectData> saveData = (List<ObjectData>)bf.Deserialize(stream);

        foreach (var obj in saveData)
        {
            Debug.Log("Loading " + obj.prefab);
            Vector3 pos = obj.transform.position;
            Quaternion rot = obj.transform.rotation;

            GameObject newObject = null;
            switch (obj.prefab)
            {
                case "Player":
                    if (onNetwork)
                    {
                        //FIXME: This is a temporary hack. Won't work on server.
                        //Player authentication must be done first.
                        playerPos = pos;
                        playerRot = rot;
                        playerData = obj.data;
                    }
                    else
                        newObject = Instantiate(playerPrefab, pos, rot);
                    break;
                case "Ship":
                    var ship = GetComponent<ResourceBank>().ship;
                    newObject = Instantiate(ship, pos, rot);
                    break;
                case "Planet":
                    var planet = GetComponent<ResourceBank>().planet;
                    newObject = Instantiate(planet, pos, rot);
                    break;
                case "Sun":
                    GameObject.Find("Sun").transform.rotation = rot;
                    break;
            }

            if (newObject != null)
            {
                var savable = newObject.GetComponent<ISavable>();
                if (savable != null)
                    savable.LoadObject(obj.data);
                NetworkServer.Spawn(newObject);
            }
        }

        stream.Close();

        Debug.Log("[GM] Loading finished");
    }

    public void SaveGame()
    {
        Debug.Log("[GM] SaveGame started...");

        List<ObjectData> saveData = new List<ObjectData>();
        var stream = GetSaveFileStream("save");

        //TODO: Save seed, version and other generic info about save

        List<GameObject> rootObjects = new List<GameObject>();
        Scene scene = SceneManager.GetActiveScene();
        scene.GetRootGameObjects( rootObjects );

        foreach (var obj in rootObjects)
        {
            var savable = obj.GetComponent<ISavable>();
            if (savable != null)
                saveData.Add(savable.SaveObject());
        }

        BinaryFormatter bf = new BinaryFormatter();  
        var ms = new System.IO.MemoryStream();  
        bf.Serialize(ms, saveData);

        var bytes = ms.ToArray();
        stream.Write(bytes, 0, bytes.Length);

        Debug.Log("[GM] SaveGame finished!");
    }

    public static void SaveFile(string file, string data)
    {
        string path = Path.Combine(saveFolder, file);
        Debug.Log("Saving to file: " + path);
        var stream = File.OpenWrite(path);
        StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.UTF8);
        writer.Write(data);
        writer.Flush();
        writer.Close();
    }

    public static bool SaveFileExists(string file)
    {
        string path = Path.Combine(saveFolder, file);
        return File.Exists(path);
    }

    public static Stream GetSaveFileStream(string file)
    {
        string path = Path.Combine(saveFolder, file);
        if (File.Exists(path))
            File.Delete(path);
        return File.OpenWrite(path);
    }

    public static Stream GetLoadFileStream(string file)
    {
        string path = Path.Combine(saveFolder, file);
        return File.OpenRead(path);
    }

    public static string GetRandomID(int length)
    {
        //FIXME: Make sure ids are unique
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[rnd.Next(0, s.Length)]).ToArray());
    }
}
