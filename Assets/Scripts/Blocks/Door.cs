﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Door : Activable
{
    public Animator animator;
    bool open;
    Properties properties;

    void Awake()
    {
        properties = GetComponent<Properties>();
        if(properties == null)
            properties = GetComponentInParent<Properties>();
        else
        {
            properties.Add(new BlockProp("Open", PropType.Bool, open.ToString()));
            properties.OnPropertyChanged += ApplyProperty;
        }
    }

    public override void Activate(Player player)
    {
        open = !open;
        properties.SetProperty("Open", open.ToString());
        animator.SetBool("Open", open);
    }

    void ApplyProperty(BlockProp p)
    {
        if(p.name == "Open")
        {
            open = bool.Parse(p.value);
            animator.SetBool("Open", open);
        }
    }
}
