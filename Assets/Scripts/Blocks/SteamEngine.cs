﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RotatingPowerSource))]
public class SteamEngine : Block, IActivable
{
    public GameObject UIPrefab;
    public RotatingPowerSource powerSource;

    Inventory fuelInventory = new Inventory(2,2);
    Image progress;
    float burnStart = 0f;
    float burnEnd = 0f;
    bool burning;
    InventoryUI invui;
    
    void Update()
    {
        powerSource.running = burning;
        if(Time.time < burnEnd)
        {
            //Update progress
            if(progress != null)
                progress.fillAmount = (Time.time - burnStart) / (burnEnd - burnStart);
        }
        else
        {
            burning = false;
            //Burn next item
            foreach (var i in fuelInventory.items)
            {
                if (i == null)
                    continue;
                
                foreach (var fuel in ResourceBank.fuelTimes.Keys)
                {
                    if(i.Matches(Item.Deserialize(fuel)))
                    {
                        burning = true;
                        fuelInventory.ModifyStack(i, -1);
                        if (invui != null)
                            invui.UpdateUI();
                        burnStart = Time.time;
                        burnEnd = burnStart + ResourceBank.fuelTimes[fuel];
                        break;
                    }
                }
                if (burning) break;  
            }          
        }
    }

    public void Activate(Player p)
    {
        Debug.Log("SteamEngine activated");
        var ui = Instantiate(UIPrefab, p.hud.transform);
        invui = ui.GetComponentInChildren<InventoryUI>();
        progress = ui.transform.Find("Progress").GetComponent<Image>();
        invui.inventory = fuelInventory;
        invui.UpdateUI();
        p.input.ShowInventory(false, ui);
    }

    public override ObjectData SaveBlock()
    {
        var obj = base.SaveBlock();
        obj.data = new Dictionary<string, string>();
        obj.data.Add("fuelInventory", fuelInventory.Serialize());
        return obj;
    }

    public override void LoadBlock(Dictionary<string, string> data)
    {
        foreach (string key in data.Keys)
        {
            switch (key)
            {
                case "fuelInventory":
                    fuelInventory.Deserialize(data[key]);
                    break;
            }
        }
    }
}
