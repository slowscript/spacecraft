﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnergyComponent))]
public class BasicElectricAppliance : Block, IElectricAppliance
{
    public float power;
    public Behaviour activeComponent;
    
    EnergyComponent energyComponent;

    void Awake()
    {
        energyComponent = GetComponent<EnergyComponent>();
        energyComponent.type = EnergyComponentType.Consumer;
        energyComponent.inputNeed = power;
    }

    public void OnPoweredOn()
    {
        activeComponent.enabled = true;
    }
    public void OnPoweredOff()
    {
        activeComponent.enabled = false;
    }
}
