﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Furnace : Block, IActivable
{
    public GameObject UIPrefab;
    public float efficiency = 1f;
    private Inventory inventory = new Inventory(2,2);
    private Inventory fuelInventory = new Inventory(2,1);
    private Slot output; 
    private Item outItem;
    private Image progress;
    private Image burningProgress;
    private GameObject ui;

    float timeElapsed = 0f;
    float timeRequired = 0f;
    bool inProgress = false;
    float burnStart = 0f;
    float burnEnd = 0f;
    bool burning = false;
    Item itemInProgress;
    void Update()
    {
        if(output != null)
            outItem = output.item; //Sync outItem before accessing it

        if (burning)
        {
            if(burningProgress != null)
                burningProgress.fillAmount = (Time.time - burnStart) / (burnEnd - burnStart);
            if (Time.time > burnEnd)
                burning = false;
        }
        else if (inProgress || !inventory.IsEmpty())
        {
            foreach (var i in fuelInventory.items)
            {
                if (i == null)
                    continue;
                
                bool match = false;
                foreach (var fuel in ResourceBank.fuelTimes.Keys)
                {
                    if(i.Matches(Item.Deserialize(fuel)))
                    {
                        match = true;
                        fuelInventory.ModifyStack(i, -1);
                        UpdateInventories();
                        burnStart = Time.time;
                        burnEnd = burnStart + ResourceBank.fuelTimes[fuel];
                        burning = true;
                        break;
                    }
                }
                if (match) break;
            }
        }

        if(!burning)
            return;

        if (inProgress)
        {
            timeElapsed += Time.deltaTime;
            if(progress != null)
                progress.fillAmount = timeElapsed / timeRequired;
            if (timeElapsed >= timeRequired)
            {
                if (outItem == null)
                    outItem = itemInProgress.Clone();
                else if (outItem.CanAdd(itemInProgress))
                    outItem.stackCount++;
                else
                    Debug.LogError("Smelted item does not match out item. This is a bug.");
                inProgress = false;
                if(output!=null)
                    output.SetItem(outItem);
            }
        }
        else
        {
            //Find item with suitable recipe
            Item itm = null;
            CraftingRecipe recipe = new CraftingRecipe();
            bool recipeFound = false;
            foreach (var i in inventory.items)
            {
                if (i == null)
                    continue;
                //Find suitable recipe
                recipe = Crafting.FindRecipeForItem(i, CraftingTool.Furnace);
                if (!recipe.Equals(default(CraftingRecipe)))
                {
                    recipeFound = true;
                    itm = i;
                    break;
                }
            }
            if (!recipeFound)
                return;
            itemInProgress = ResourceBank.GetItem(recipe.product);

            //If result can fit into output slot, start smelting
            if (outItem == null || outItem.CanAdd(itemInProgress))
            {
                inventory.ModifyStack(itm, -1);
                UpdateInventories();
                inProgress = true;
                timeElapsed = 0;
                timeRequired = recipe.time * efficiency;
            }
        }
    }

    void UpdateInventories()
    {
        if(ui != null)
            ui.GetComponentsInChildren<InventoryUI>().ToList().ForEach(i => i.UpdateUI());
    }

    public void Activate(Player player)
    {
        Debug.Log("Furnace activated");
        ui =  Instantiate(UIPrefab, player.hud.transform);
        var inv = ui.GetComponentsInChildren<InventoryUI>();
        progress = ui.transform.Find("Progress").GetComponent<Image>();
        burningProgress = ui.transform.Find("BurningProgress").GetComponent<Image>();
        Debug.Assert(inv.Length == 2, "Furnace UI does not have required amount of inventories.");
        inv[0].inventory = inventory;
        inv[0].UpdateUI();
        inv[1].inventory = fuelInventory;
        inv[1].UpdateUI();
        output = ui.transform.Find("Item").GetComponent<Slot>();
        output.SetItem(outItem);
        player.input.ShowInventory(false, ui);
    }

    public override ObjectData SaveBlock()
    {
        var obj = base.SaveBlock();
        obj.data = new Dictionary<string, string>();
        obj.data.Add("inventory", inventory.Serialize());
        obj.data.Add("fuelInventory", fuelInventory.Serialize());
        obj.data.Add("outItem", outItem == null ? "e" : outItem.Serialize());
        return obj;
    }

    public override void LoadBlock(Dictionary<string, string> data)
    {
        foreach (string key in data.Keys)
        {
            switch (key)
            {
                case "inventory":
                    inventory.Deserialize(data[key]);
                    break;
                case "fuelInventory":
                    fuelInventory.Deserialize(data[key]);
                    break;
                case "outItem":
                    outItem = Item.Deserialize(data[key]);
                    break;
            }
        }
    }
}
