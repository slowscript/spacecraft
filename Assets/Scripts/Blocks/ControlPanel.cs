﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPanel : Activable
{
    bool playerIsMounted = false;
    Player mountedPlayer = null;

    public override void Activate(Player player)
    {
        if (!playerIsMounted)
        {
            Vector3 mountPos = transform.position - transform.forward;
            bool freeSpace = !Physics.CheckSphere(mountPos, 0.4f);
            if (freeSpace)
            {
                playerIsMounted = true;
                mountedPlayer = player;
                mountedPlayer.controller.MountShip(transform, mountPos);
            }
            else
                player.hud.DisplayMessage("Cannot mount " + name);
        }
        else
        {
            mountedPlayer.controller.DismountShip();
            playerIsMounted = false;
            mountedPlayer = null;
            player.hud.DisplayMessage("Dismounted " + name);
        }
    }
}
