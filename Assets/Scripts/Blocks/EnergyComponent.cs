﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnergyComponentType { Producer, Consumer, Storage };

//A component used as an interface between Block scripts and the energy system
//!!Should not be modified in the editor!!
[RequireComponent(typeof(IElectricAppliance))]
public class EnergyComponent : MonoBehaviour
{
    public string note = "This shouldn't be modified in the editor. Variables in this component are handled by this block's scripts and the energy system.";
    public EnergyComponentType type;

    //Not needed for producer
    public float inputNeed;
    //Not needed for consumer
    public float maxOutput;
    
    public float currentOutput; //For the future - smart generators will be able to regulate production
                                //(and consumption of fuel), others will ignore it

    //Gives player ability to manually turn off certain devices
    public bool isTurnedOn = true;
    //Internal variable, tells whether this component is running and consuming energy
    [SerializeField]
    private bool isPoweredOn;

    private IElectricAppliance appliance;

    void Awake()
    {
        appliance = GetComponent<IElectricAppliance>();
        appliance.OnPoweredOff(); //Default state is off
    }

    //This method is called every update of the energy system on all consumers that are turned on
    public void PowerUpdate(bool isOn)
    {
        bool newState = isOn && isTurnedOn;
        if (isPoweredOn && !newState) //If was powered on and now should power off
            appliance.OnPoweredOff();
        
        if (!isPoweredOn && newState) //If should be powered on and is turned on
            appliance.OnPoweredOn();

        isPoweredOn = newState;
    }

    public void TurnOnOff(bool isOn)
    {
        isTurnedOn = isOn;
        if (!isTurnedOn)
            PowerUpdate(false); //Just power off. It gets powered on automatically by the energy system.
    }
}
