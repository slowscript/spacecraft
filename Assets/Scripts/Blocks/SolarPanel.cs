﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnergyComponent))]
public class SolarPanel : Block
{
    public float maxPower = 250f;

    [SerializeField] float checkDistance = 100f;
    [SerializeField] float efficiency;

    EnergyComponent energyComponent;
    Transform sun;

     void Awake()
    {
        energyComponent = GetComponent<EnergyComponent>();
        energyComponent.type = EnergyComponentType.Producer;
        energyComponent.maxOutput = maxPower;
        energyComponent.isTurnedOn = true;
        sun = GameObject.Find("Sun").transform;
    }

    void Update()
    {
        //Check if sky is clear
        if(Physics.Raycast(transform.position, -sun.forward, checkDistance))
        {
            energyComponent.maxOutput = 0;
            return;
        }
        
        var angle = Vector3.Angle(-transform.up, sun.forward);
        efficiency = Mathf.Clamp01(Mathf.Cos(Mathf.Deg2Rad * angle));
        energyComponent.maxOutput = maxPower * efficiency;
    }
}
