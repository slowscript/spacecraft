﻿using System.Linq;
using UnityEngine;

[RequireComponent(typeof(EnergyComponent))]
public class ElectricMotor : Block
{
    public bool isGenerator = false;
    public float maxPower = 1000f;
    public int[] engineIDs;
    
    //Only in motor mode
    [Range(0f, 1f)] public float dutyCycle = 0f;

    EnergyComponent energyComponent;
    RotatingPowerSource powerSource;

    void Awake()
    {
        energyComponent = GetComponent<EnergyComponent>();
        energyComponent.maxOutput = maxPower;
        energyComponent.isTurnedOn = true;
    }

    void Update()
    {
        if(isGenerator)
        {
            energyComponent.type = EnergyComponentType.Producer;
            energyComponent.maxOutput = powerSource.running ? Mathf.Min(powerSource.maxPower, maxPower) : 0f;
        }
        else
        {
            energyComponent.type = EnergyComponentType.Consumer;
            energyComponent.inputNeed = maxPower * dutyCycle;
        }
    }

    public override void OnUpdateShip()
    {
        //Check if motor is connected to an engine, if yes, act as a generator
        if(Physics.Raycast(transform.position, transform.right, out RaycastHit hit , 0.6f))
        {
            if(hit.collider.tag == "Buildable")
            {
                var block = hit.collider.GetComponent<Block>();
                isGenerator = engineIDs.Contains(block.blockID) && (block.transform.right == transform.right);;
                if(isGenerator)
                    powerSource = block.GetComponent<RotatingPowerSource>();
            }
        }
    }
}
