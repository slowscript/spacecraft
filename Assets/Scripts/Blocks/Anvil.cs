﻿using System.Collections.Generic;
using UnityEngine;

public class Anvil : Block, IActivable
{
    public void Activate(Player player)
    {
        Debug.Log("Anvil activated");
        Player.local.hud.craftingPanel.toolsEnabled = new List<CraftingTool> { CraftingTool.Anvil};
        Player.local.input.ShowInventory(true);
    }
}
