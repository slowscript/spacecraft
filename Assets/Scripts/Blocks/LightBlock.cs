﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnergyComponent))]
public class LightBlock : Block, IElectricAppliance
{
    public float power;
    public Behaviour[] lights;
    public Renderer renderer;
    public Material glow;
    public Material glass;
    public int materialSlot;
    
    EnergyComponent energyComponent;

    void Awake()
    {
        energyComponent = GetComponent<EnergyComponent>();
        energyComponent.type = EnergyComponentType.Consumer;
        energyComponent.inputNeed = power;
    }

    public void OnPoweredOn()
    {
        foreach (var l in lights)
            l.enabled = true;

        var mats = renderer.materials;
        mats[materialSlot] = glow;
        renderer.materials = mats;
    }
    public void OnPoweredOff()
    {
        foreach (var l in lights)
            l.enabled = false;

        var mats = renderer.materials;
        mats[materialSlot] = glass;
        renderer.materials = mats;
    }
}
