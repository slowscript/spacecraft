﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : Block, IActivable
{
    public GameObject UIPrefab;
    private Inventory inventory = new Inventory(10,3);

    public void Activate(Player player)
    {
        Debug.Log("Container opened");
        var ui =  Instantiate(UIPrefab, player.hud.transform);
        var inv = ui.GetComponent<InventoryUI>();
        inv.inventory = inventory;
        inv.UpdateUI();
        player.input.ShowInventory(false, ui);
    }

    public override ObjectData SaveBlock()
    {
        var obj = base.SaveBlock();
        obj.data = new Dictionary<string, string>();
        obj.data.Add("inventory", inventory.Serialize());
        return obj;
    }

    public override void LoadBlock(Dictionary<string, string> data)
    {
        foreach (string key in data.Keys)
        {
            if (key == "inventory")
            {
                inventory.Deserialize(data[key]);
                return;
            }
        }
    }
}
