﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeldingFrame : Block, IActivable
{
    public float maxDistance = 0.65f; //from center to other's surface -> 0.15 gap
    public float angleTreshold = 10; //10 degrees

    public void Activate(Player p) => Weld();

    public void Weld()
    {
        if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, maxDistance))
        {
            var obj = hit.collider.transform;
            if (obj.CompareTag("Buildable") && (obj.GetComponent<WeldingFrame>() != null) && (obj.root != transform.root))
            {
                //TODO: Detect if other is a sub-ship (on a piston etc.) and disallow merging this way (only other way)
                if ((Vector3.Angle(transform.forward, -obj.forward) < angleTreshold) &&
                    (Vector3.Distance(transform.position, obj.position) < maxDistance + 0.5f))
                {
                    var rot = Quaternion.FromToRotation(obj.forward, -transform.forward);
                    obj.root.rotation *= rot;
                    var tr = transform.position + transform.forward - obj.position;
                    obj.root.position += tr;
                    
                    //Reparent everything
                    var origRoot = obj.root;
                    foreach (Transform child in obj.root.GetComponentsInChildren<Transform>())
                        if(child.parent == origRoot)
                            child.parent = transform.parent;
                    
                    Destroy(origRoot.gameObject);
                }
                else Player.local.hud.DisplayMessage("Cannot weld objects that aren't well aligned");
            } else Player.local.hud.DisplayMessage("Can only weld to other Welding Frame on a different ship");
        } else Player.local.hud.DisplayMessage("There is nothing to weld to in front of the frame");
    }
}
