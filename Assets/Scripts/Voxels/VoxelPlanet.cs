﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelPlanet : MonoBehaviour {

    public int planetDiameter = 100;
    public int baseChunks = 1;
    public GameObject chunkPrefab;

    List<Transform> chunks = new List<Transform>();

	// Use this for initialization
	void Start () {
        for (int x = -baseChunks; x < baseChunks; x++)
        {
            for (int y = -baseChunks; y < baseChunks; y++)
            {
                for (int z = -baseChunks; z < baseChunks; z++)
                {
                    GameObject child = Instantiate(chunkPrefab, transform, false);
                    child.transform.localPosition = new Vector3(x * VoxelChunk.chunkSize, y * VoxelChunk.chunkSize, z * VoxelChunk.chunkSize);
                    var v = child.GetComponent<VoxelChunk>(); v.LoadChunk(); v.RenderChunk();
                }
            }
        }
        //GameObject child = Instantiate(chunkPrefab, transform, false);
        ////child.transform.localPosition = new Vector3(x * VoxelChunk.chunkSize, y * VoxelChunk.chunkSize, z * VoxelChunk.chunkSize);
        //var v = child.GetComponent<VoxelChunk>(); v.LoadChunk(); v.RenderChunk();
    }

    //Uses floored local coordinates to get chunk
    public Transform GetChunkForCoordinates(int x, int y, int z)
    {
        foreach (var c in chunks)
        {
            if (c.localPosition == new Vector3(x, y, z))
                return c;
        }
        return null;
    }
}
