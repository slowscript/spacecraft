﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Profiling;

public enum ChunkState
{
    NotGenerated, Loaded, Unloaded, Hosted    
}

public class ChunkTemplate
{
    public string ID;
    public int numID;
    public bool isServer;
    public ChunkState initialState;
    public Vector3 position;
    public Quaternion rotation;
    public Transform parent;
    public PlanetObject planet;
    public PlanetChunk.HeightMapData heightData;
    public Sides side;
    public PlanetChunk instance = null;
    public Vector3 center;

    public void CalculateCenter()
    {
        //Isn't exactly the same as PlanetChunk.centerPoint, but close enough
        var chunkLocPos = Quaternion.Inverse(rotation) * (position - parent.position);
        center = (rotation * (PlanetChunk.MapToSphere(Vector3.one * Planet.chunkSize / 2, chunkLocPos, planet.standardHeight) + chunkLocPos)) + parent.position;
    }
}

#pragma warning disable CS0618 //Hide all the UNET deprecation warnings
public class PlanetChunk : MonoBehaviour
{
    [System.Serializable()]
    public struct HeightMapData
    {  
        public int x, y, h;
    }

    public ChunkState chunkState = ChunkState.NotGenerated;
    public bool saved = false;
    public int numID;
    public string ID;
    public bool isHost;
    	
    //Size of a voxel in "standardHeight"
	public float voxelSize = 1f; //<-- NOT IMPLEMENTED YET

	public int LOD;
    public float unloadTolerance = 50f;
    private int lodSize;
    private byte[] blocks;
    public Vector3 centerPoint;
    private Vector3 chunkOffset;
    private Vector3 chunkRotation;

    private Vector3[] verts;
    private int[] tris;
    private Vector2[] UVs;

    private Mesh mesh;
    private MeshCollider coll;

    private float damageTreshold = 30f;
    private HeightMapData heightData;
    private Sides side;
    public PlanetObject planet;
    private bool retrievingData = false;
    private bool loadingInProgress = false;
    private object loadLock = new object();
    private byte[] loadedBlocks;
    private MeshBuilderJob builderJob;
    private JobHandle builderJobHandle;
    bool builderJobStarted = false;

    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.MarkDynamic();
        coll = GetComponent<MeshCollider>();
    }

    void OnEnable()
    {
        World.chunks.Add(this);
    }

    void OnDisable()
    {
        World.chunks.Remove(this);
    }

    void OnCollisionEnter(Collision col) 
    {
        //Forward to Planet
        planet.OnCollisionEnter(col);
    }

    public void InitFromTemplate(ChunkTemplate template)
    {
        numID = template.numID;
        ID = template.ID;
        isHost = template.isServer;
        planet = template.planet;
        chunkState = template.initialState;
        if (chunkState == ChunkState.Unloaded)
            saved = true;
        heightData = template.heightData;
        side = template.side;

        template.instance = this;

        LOD = World.LODSteps.Length + 1;
        chunkOffset = transform.localPosition;
        chunkRotation = transform.rotation.eulerAngles;
        centerPoint = transform.TransformPoint(MapToSphere(Vector3.one * Planet.chunkSize / 2));
    }

    public void VisibilityUpdate(float dist)
    {
        UnityEngine.Profiling.Profiler.BeginSample("VisibilityUpdate");
        bool buildMeshRequest = false;
        
        /*** Apply asynchronous results ***/
        lock (loadLock)
        {
            if (loadedBlocks != null)
            {
                loadingInProgress = false;
                blocks = loadedBlocks;
                loadedBlocks = null;
                saved = true;
                chunkState = ChunkState.Loaded;
                buildMeshRequest = true;
            }
        }

        if(builderJobStarted)
        {
            builderJobStarted = false;

            builderJobHandle.Complete();
            verts = builderJob.verts.ToArray();
            tris = builderJob.tris.ToArray();
            UVs = builderJob.UVs.ToArray();
            builderJob.blocks.Dispose();
            builderJob.verts.Dispose();
            builderJob.tris.Dispose();
            builderJob.UVs.Dispose();
            ApplyMeshData();
        }

        /*** Update LOD value ***/
        int newLod = World.LODSteps.Length + 1;

        for (int i = 0; i < World.LODSteps.Length; i++)
        {
            if (dist < World.LODSteps[i])
            {
                newLod = i + 1;
                break;
            }
        }

        //Apply new LOD
        if (newLod != LOD)
        {
            //Load chunk data if needed
            if ((newLod <= World.LODSteps.Length) && (chunkState != ChunkState.Loaded))
            {
                switch (chunkState)
                {
                    case ChunkState.Hosted:
                        if (!retrievingData)
                        {
                            retrievingData = true;
                            NetworkHelper.current.CmdRequestData(GameManager.GetConnectionToServer(), ID);
                        }
                        break;
                    case ChunkState.Unloaded:
                        ScheduleLoadChunk();
                        break;
                }
            }
            else
            {
                LOD = newLod;
                lodSize = Planet.chunkSize / LOD;
                buildMeshRequest = true;
            }
        }

        /*** Update mesh if needed ***/
        if (buildMeshRequest)
            RebuildMesh();

        UnityEngine.Profiling.Profiler.EndSample();
    }

    public async void RequestData(int connID)
    {
        if(chunkState == ChunkState.Unloaded)
            await Task.Run(() => LoadChunk());
        
        //Build message and send data
        var msg = ChunkDataMsg.Create(blocks, ID);
        NetworkServer.SendToClient(connID, ChunkDataMsg.id, msg);
    }

    public void OnDataReceive(byte[] data)
    {
        SetBlocks(data);
        chunkState = ChunkState.Loaded;
        retrievingData = false;
        RebuildMesh();
    }

    void ProcessCollision(Collision c)
    {
        var damage = c.impulse.magnitude / 5;
        print("Damage to voxels: " + damage);
        if(damage > damageTreshold)
        {
            foreach(var p in c.contacts)
            {
                DestroyAtContact(p);
            }
        }
    }

    public void SetBlocks(byte[] _blocks)
    {
        if (_blocks.Length == Planet.chunkSize * Planet.chunkSize * Planet.chunkSize)
        {
            blocks = _blocks;
            saved = false;
        }
        else Debug.LogError("Cannot set block array of different size than chunkSize");
    }

    public void UpdateBlock(int x, int y, int z, byte value)
    {
        if (chunkState == ChunkState.Loaded)
        {
            if ((x >= 0) && (x < Planet.chunkSize) && (y >= 0) && (y < Planet.chunkSize) && (z >= 0) && (z < Planet.chunkSize))
            {
                SetBlock(x, y, z, value);
                saved = false;
            }
        }
        else
            Debug.LogWarning("UpdateBlock on not loaded chunks prohibited!");
    }

    void SetBlock(int x, int y, int z, byte value)
    {
        blocks[x + Planet.chunkSize * (y + Planet.chunkSize * z)] = value;
    }

    public byte GetBlockAtHit(RaycastHit hit)
    {
        var coords = GetCoordsAtHit(hit);
        return GetBlock(coords);
    }

    public Vector3 GetCoordsAtHit(RaycastHit hit)
    {
        var pt = GetPointAtHit(hit);
        return MapBackToCube(pt).Floor();
    }

    private Vector3 GetPointAtHit(RaycastHit hit)
    {
        var pt = transform.InverseTransformPoint(hit.point);
        var locNorm = hit.transform.InverseTransformDirection(hit.normal);
        var targetPoint = pt - (locNorm / 2);
        return targetPoint;
    }
    
    public int DestroyAtHit(RaycastHit hit)
    {
        var targetPoint = GetPointAtHit(hit);
        return DestroyVoxel(targetPoint);
    }

    public int DestroyAtContact(ContactPoint cpt)
    {
        var pt = transform.InverseTransformPoint(cpt.point);
        var locNorm = transform.InverseTransformDirection(cpt.normal);
        var targetPoint = pt - (locNorm / 2);
        
        return DestroyVoxel(targetPoint);
    }

    public int DestroyVoxel(Vector3 target)
    {
        var coords = MapBackToCube(target).Floor();

        int id = GetBlock(coords);
        if (id != 7) //Dont destroy bedrock
        {
            NetworkHelper.current.CmdUpdateVoxel(ID, (int)coords.x, (int)coords.y, (int)coords.z, 0);
            return id;
        }
        else return -1;
	}

    public void PlaceVoxel(RaycastHit hit, byte id)
    {
        var pt = hit.point + (hit.normal / 4);
        PlaceVoxel(pt, id);
    }

    public void PlaceVoxel(Vector3 point, byte id, bool recursion = false)
    {        
        var targetPoint = transform.InverseTransformPoint(point);
        var coords = MapBackToCube(targetPoint).Floor();
        PlanetChunk correctChunk;
        if (!IsCorrectChunk(coords, out correctChunk))
        {
            if(recursion)
            {
                Debug.LogWarning("Failed to identify correct chunk to put voxel in. Avoiding infinite recursion.");
                return;
            }
            correctChunk.PlaceVoxel(point, id, true);
        }
        else PlaceVoxelAt(coords, id);
    }

    public void PlaceVoxelAt(Vector3 coords, byte id)
    {
        NetworkHelper.current.CmdUpdateVoxel(ID, (int)coords.x, (int)coords.y, (int)coords.z, id);
    }

    void ScheduleLoadChunk()
    {
        if (loadingInProgress)
            return;
        loadingInProgress = true;
        ThreadPool.QueueUserWorkItem(LoadChunk);
    }

    private void LoadChunk(object o = null)
    {
        if (GameManager.SaveFileExists(ID))
        {
            using (var stream = GameManager.GetLoadFileStream(ID))
            {
                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    lock(loadLock)
                        loadedBlocks = ms.ToArray();
                }
            }
        }
        else
        {
            DoApplyHeightMap();
        }
    }

    public void UnloadChunk()
    {
        //if server
        if (isHost)
        {
            SaveChunk();
            chunkState = ChunkState.Unloaded;
        }
        else chunkState = ChunkState.Hosted;
        blocks = null;
        mesh.Clear();
    }

    public void SaveChunk()
    {
        if (saved)
            return;
        using (var stream = GameManager.GetSaveFileStream(ID))
        {
            stream.Write(blocks, 0, blocks.Length);
        }

        saved = true;
    }

    private void DoApplyHeightMap()
    {
        Profiler.BeginSample("DoApplyHeightMap");
        var newBlocks = new byte[Planet.chunkSize * Planet.chunkSize * Planet.chunkSize];
        int hx = heightData.x * Planet.chunkSize;
        int hy = heightData.y * Planet.chunkSize;
        int hh = heightData.h * Planet.chunkSize;
        float[,] hMap = planet.GetHeightMap(side);
        if (hMap == null) //Not generated yet -> abort, will be tried again in next frame
        {
            loadingInProgress = false;
            return;
        }
        BiomeType[,] bMap = planet.biomeMaps[(int)side];
        for (int x = 0; x < Planet.chunkSize; x++)
        {
            for (int y = 0; y < Planet.chunkSize; y++)
            {
                for (int z = 0; z < Planet.chunkSize; z++)
                {
                    float val = hMap[hx + x, hy + z];
                    var biome = Biomes.biomeList[(int)bMap[hx + x, hy + z]];
                    val *= (planet.maxHeight - planet.baseHeight) / 2;
                    val += planet.baseHeight;
                    if (y + hh == planet.baseHeight)
                    {
                        newBlocks[x + Planet.chunkSize * (y + Planet.chunkSize * z)] = 7; //Bedrock
                    }
                    else
                    {
                        if (y + hh < val && y + hh > val - 1) //Top layer -> generate flora
                        {
                            //Trees
                            if(bMap[hx + x, hy + z] == BiomeType.Forest)
                                planet.treeBuilder.Build(x, y, z, ref newBlocks, this);
                        }
                        foreach (var layer in biome.layers)
                        {
                            if (y + hh < val - layer.minDepth)
                            {
                                if (layer.voxel == 1) //Stone
                                {
                                    //Generate ores in stone
                                    var pos = BlockToGlobalPos(x, y, z);
                                    
                                    newBlocks[x + Planet.chunkSize * (y + Planet.chunkSize * z)] = 1; //Stone
                                    //Overwrite stone with ore if it's supposed to generate here
                                    foreach (var ore in planet.data.ores)
                                    {
                                        if (ore.GenerateAt(pos.x, pos.y, pos.z))
                                            newBlocks[x + Planet.chunkSize * (y + Planet.chunkSize * z)] = ore.block; //Ore 
                                    }
                                }
                                else
                                    newBlocks[x + Planet.chunkSize * (y + Planet.chunkSize * z)] = layer.voxel;

                                break;
                            }
                        }
                    }
                }
            }
        }
        lock (loadLock)
            loadedBlocks = newBlocks;
        Profiler.EndSample();
    }

    public Vector3 BlockToGlobalPos(int x, int y, int z)
    {
        Vector3 pos = MapToSphere(new Vector3(x, y, z));
        pos = pos.RotateAroundPivot(Vector3.zero, chunkRotation);
        return pos;
    }

    public void RebuildMesh()
    {
        if(builderJobStarted)
            return;
        
        builderJobStarted = true;
        builderJob = new MeshBuilderJob()
        {
            blocks = new NativeArray<byte>(blocks, Allocator.Persistent),
            stdHeight = planet.standardHeight,
            LOD = LOD,
            lodSize = lodSize,
            chunkOffset = chunkOffset,
            verts = new NativeList<Vector3>(15000, Allocator.Persistent),
            tris = new NativeList<int>(15000, Allocator.Persistent),
            UVs = new NativeList<Vector2>(15000, Allocator.Persistent)
        };
        builderJobHandle = builderJob.Schedule();
    }

    void ApplyMeshData()
    {
        UnityEngine.Profiling.Profiler.BeginSample("ApplyMeshData");
        mesh.Clear();

        mesh.vertices = verts;
        mesh.uv = UVs;
        mesh.triangles = tris;

        mesh.RecalculateNormals();

        coll.sharedMesh = null;
        coll.sharedMesh = mesh;

        UnityEngine.Profiling.Profiler.EndSample();
    }

    public byte GetBlock(Vector3 v)
    {
        return GetBlock((int)v.x, (int)v.y, (int)v.z);
    }

    public byte GetBlock(int x, int y, int z)
    {
        if (CoordsValid(x, y, z))
            return Block(x, y, z);
        else return 0;
    }

    private byte Block(int x, int y, int z)
    {
        return blocks[x + Planet.chunkSize * (y + Planet.chunkSize * z)];
    }

	private byte GetLODBlock(int x, int y, int z)
	{
        if ((x >= 0) && (x < lodSize) && (y >= 0) && (y < lodSize) && (z >= 0) && (z < lodSize))
			return Block(x*LOD, y*LOD, z*LOD);
		else return 0;
	}
        
    /* 
     * Experimental. Tries to include neighboring chunks to decrease polygon count
     * but it doesn't seem to speed up anything and only causes trouble.
     * I kept the function, because it may be useful later (ore generation etc.)
     */
    private byte CheckLODBlock(int x, int y, int z)
    {
        if ((x >= 0) && (x < lodSize) && (y >= 0) && (y < lodSize) && (z >= 0) && (z < lodSize))
            return Block(x*LOD, y*LOD, z*LOD);
        else
        {
            var targetChunkPos = transform.localPosition;
            if (x < 0) targetChunkPos.x -= Planet.chunkSize;
            if (y < 0) targetChunkPos.y -= Planet.chunkSize;
            if (z < 0) targetChunkPos.z -= Planet.chunkSize;
            if (x >= lodSize) targetChunkPos.x += Planet.chunkSize;
            if (y >= lodSize) targetChunkPos.y += Planet.chunkSize;
            if (z >= lodSize) targetChunkPos.z += Planet.chunkSize;

            Transform targetChunk = null;
            foreach (Transform chunk in transform.parent)
            {
                if (chunk.localPosition == targetChunkPos)
                {
                    targetChunk = chunk;
                    //Debug.Log("ChunkFound");
                    break;
                }
            }
            if (targetChunk != null)
            {
                int x2 = Mathf.Abs(Mathf.Abs(x * LOD) - Planet.chunkSize);
                //int y2 = Mathf.Abs(Mathf.Abs(y * LOD) - Planet.chunkSize);
                int z2 = Mathf.Abs(Mathf.Abs(z * LOD) - Planet.chunkSize);
                //for(int _x, _y, _z; x )
                //Debug.Log("Checking coords " + x2 + " " + y + " " + z2);
                return targetChunk.GetComponent<PlanetChunk>().GetBlock(x2, y, z2);
            }
            else
                return 0;
        }
    }

    private bool CoordsValid(int x, int y, int z)
    {
        return (x >= 0) && (x < Planet.chunkSize) && (y >= 0) && (y < Planet.chunkSize) && (z >= 0) && (z < Planet.chunkSize);
    }

    private bool IsCorrectChunk(Vector3 v, out PlanetChunk correctChunk)
    {
        correctChunk = null;
        if (CoordsValid((int)v.x, (int)v.y, (int)v.z))
            return true;
        
        Vector3 newPos = transform.localPosition;
        if (v.x < 0)
            newPos += new Vector3(-Planet.chunkSize, 0, 0);
        if (v.x >= Planet.chunkSize)
            newPos += new Vector3(Planet.chunkSize, 0, 0);
        if (v.y < 0)
            newPos += new Vector3(0, -Planet.chunkSize, 0);
        if (v.y >= Planet.chunkSize)
            newPos += new Vector3(0, Planet.chunkSize, 0);
        if (v.z < 0)
            newPos += new Vector3(0, 0, -Planet.chunkSize);
        if (v.z >= Planet.chunkSize)
            newPos += new Vector3(0, 0, Planet.chunkSize);
        
        var pos = transform.parent.TransformPoint(newPos);
        correctChunk = World.FindChunkAtPosition(pos);
        if (correctChunk == null) //Wrong side
        {
            Vector3 axis;
            newPos = transform.localPosition; //Reset previous unsuccessfull changes
            float max;
            if((v.x >= Planet.chunkSize) || (v.x < 0))
            {
                axis = new Vector3(0,0,1);
                max = newPos.x;
                newPos.x = -newPos.x;
            } else {
                axis = new Vector3(-1,0,0);
                max = newPos.z;
                newPos.z = -newPos.z;
            }

            var diffRot = Quaternion.AngleAxis(-Mathf.Sign(max) * 90, axis);
            var rotatedPos = diffRot * newPos;
            pos = transform.parent.TransformPoint(rotatedPos);
            correctChunk = World.FindChunkAtPosition(pos);
        }
        return false;
    }

    private Vector3 MapToSphere(Vector3 v0)
    {
        return MapToSphere(v0, chunkOffset, planet.standardHeight);
    }

    public static Vector3 MapToSphere(Vector3 v0, Vector3 chunkOffset, int stdHeight)
    {
        //CUBE to pentahedron
        v0 -= new Vector3(Planet.chunkSize / 2f, 0, Planet.chunkSize / 2f);
        v0 += chunkOffset;
        float yi = v0.y * 0.5f;
        float xi = v0.x * (v0.y / stdHeight);
        float zi = v0.z * (v0.y / stdHeight);
        Vector3 v = new Vector3(xi, yi, zi);

        //We need to start with a cube with dimensions of 2*2*2 
        var div = Mathf.Max(v.x, v.y, v.z);
        v /= div;
        //Spherigify
        float x2 = v.x * v.x;
        float y2 = v.y * v.y;
        float z2 = v.z * v.z;
        Vector3 s;
        s.x = v.x * Mathf.Sqrt(1f - y2 / 2f - z2 / 2f + y2 * z2 / 3f);
        s.y = v.y * Mathf.Sqrt(1f - x2 / 2f - z2 / 2f + x2 * z2 / 3f);
        s.z = v.z * Mathf.Sqrt(1f - x2 / 2f - y2 / 2f + x2 * y2 / 3f);
        return s.normalized * v0.y - chunkOffset;
    }
        
    private Vector3 MapBackToCube(Vector3 v0)
    {
        v0 += chunkOffset; //Get planetside-relative vector
        var y = v0.magnitude; //y coord isn't changed
        v0.Normalize();
        //Despherigification itself
        var v = Cubify(v0.xzy()).xzy();
        //Denormalize
        v *= y;
        //Reverse pentahedron mapping
        float yi = y * 2f;
        float xi = v.x / (yi / planet.standardHeight);
        float zi = v.z / (yi / planet.standardHeight);
        var result = new Vector3(xi, y, zi);
        result -= chunkOffset;
        result += new Vector3(Planet.chunkSize / 2f, 0, Planet.chunkSize / 2f);
        return result;
    }
    
    Vector3 Cubify (Vector3 s)
    {
        float xx2 = s.x * s.x * 2.0f;
        float yy2 = s.y * s.y * 2.0f;
        
        Vector2 v = new Vector2(xx2 - yy2, yy2 - xx2);
        
        float ii = v.y - 3.0f;
        ii *= ii;

        float isqrt = -Mathf.Sqrt(ii - (12.0f * xx2)) + 3.0f;

        v = v.Add(isqrt).Sqrt();
        v *= 1f / Mathf.Sqrt(2f);
        
        return new Vector3(v.x, v.y, 1.0f).Multiply(s.Sign());
    }

    struct MeshBuilderJob : IJob
    {
        public NativeList<Vector3> verts;
        public NativeList<int> tris; 
        public NativeList<Vector2> UVs;
        public NativeArray<byte> blocks;
        public int lodSize;
        public int LOD;
        public Vector3 chunkOffset;
        public int stdHeight;

        public void Execute()
        {
            RenderChunk();
        }

        void RenderChunk()
        {
            UnityEngine.Profiling.Profiler.BeginSample("RenderChunk");
            for (int x = 0; x < lodSize; x++)
            {
                for (int y = 0; y < lodSize; y++)
                {
                    for (int z = 0; z < lodSize; z++)
                    {
                        if (Block(x*LOD, y*LOD, z*LOD) != 0)
                        {
                            RenderCube(x, y, z, Block(x*LOD, y*LOD, z*LOD));
                        }
                    }
                }
            }
            CalculateTris();
            UnityEngine.Profiling.Profiler.EndSample();
        }

        void RenderCube(int x, int y, int z, byte blockType = 1)
        {
            int lx = x * LOD;
            int ly = y * LOD;
            int lz = z * LOD;
            int l1 = LOD;
            //Top
            if (GetLODBlock(x, y + 1, z) == 0)
            {
                verts.Add(MapToSphere(new Vector3(lx, ly + l1, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly + l1, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly + l1, lz)));
                verts.Add(MapToSphere(new Vector3(lx, ly + l1, lz)));
                SetUVs(blockType);
            }
            //Front
            if (GetLODBlock(x, y, z + 1) == 0)
            {
                verts.Add(MapToSphere(new Vector3(lx + l1, ly, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly + l1, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx, ly + l1, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx, ly, lz + l1)));
                SetUVs(blockType);
            }
            //Right
            if (GetLODBlock(x + 1, y, z) == 0)
            {
                verts.Add(MapToSphere(new Vector3(lx + l1, ly, lz)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly + l1, lz)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly + l1, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly, lz + l1)));
                SetUVs(blockType);
            }
            //Back
            if (GetLODBlock(x, y, z - 1) == 0)
            {
                verts.Add(MapToSphere(new Vector3(lx, ly, lz)));
                verts.Add(MapToSphere(new Vector3(lx, ly + l1, lz)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly + l1, lz)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly, lz)));
                SetUVs(blockType);
            }
            //Left
            if (GetLODBlock(x - 1, y, z) == 0)
            {
                verts.Add(MapToSphere(new Vector3(lx, ly, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx, ly + l1, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx, ly + l1, lz)));
                verts.Add(MapToSphere(new Vector3(lx, ly, lz)));
                SetUVs(blockType);
            }
            //Bottom
            if (GetLODBlock(x, y - 1, z) == 0)
            {
                verts.Add(MapToSphere(new Vector3(lx, ly, lz)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly, lz)));
                verts.Add(MapToSphere(new Vector3(lx + l1, ly, lz + l1)));
                verts.Add(MapToSphere(new Vector3(lx, ly, lz + l1)));
                SetUVs(blockType);
            }
        }

        void SetUVs(byte blockType)
        {
            UVs.Add(Planet.tilePos[blockType]);
            UVs.Add(Planet.tilePos[blockType] + new Vector2(Planet.tileSize, 0));
            UVs.Add(Planet.tilePos[blockType] + new Vector2(Planet.tileSize, Planet.tileSize));
            UVs.Add(Planet.tilePos[blockType] + new Vector2(0, Planet.tileSize));
        }

        void CalculateTris()
        {
            for (int i = 0; i < verts.Length; i += 4)
            {
                tris.Add(i); //1
                tris.Add(i + 1); //2
                tris.Add(i + 2); //3
                tris.Add(i); //1
                tris.Add(i + 2); //3
                tris.Add(i + 3); //4
            }
        }

        private Vector3 MapToSphere(Vector3 v0)
        {
            return PlanetChunk.MapToSphere(v0, chunkOffset, stdHeight);
        }
        private byte Block(int x, int y, int z)
        {
            return blocks[x + Planet.chunkSize * (y + Planet.chunkSize * z)];
        }

        private byte GetLODBlock(int x, int y, int z)
        {
            if ((x >= 0) && (x < lodSize) && (y >= 0) && (y < lodSize) && (z >= 0) && (z < lodSize))
                return Block(x*LOD, y*LOD, z*LOD);
            else return 0;
        }
    }
}
