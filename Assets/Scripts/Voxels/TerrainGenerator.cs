﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LibNoise;

public class TerrainGenerator
{
    IModule3D perlin;
    float scale;

    public TerrainGenerator(int seed, float _scale) //Also pass planet type
    {
        perlin = new LibNoise.Primitive.SimplexPerlin(seed, NoiseQuality.Standard);
        scale = _scale;
    }

    // Returns noise value for point xyz in range 0...2
    public float GetHeight(float x, float y, float z, out BiomeType biome)
    {
        x *= scale;
        y *= scale;
        z *= scale;
        float height = 0f;

        //Determine ocean and land
        float landMask = Noise(x, y, z, 4, 0.04f, 0.8f);
        landMask = (Clamp(landMask, -0.2f, 0f) + 0.2f) * 5f; //gradient: 0=ocean 1=land

        float landMap = 0f;
        float mountainMask = 0f;
        if (landMask > 0f)
        {
            landMap = Noise(x, y, z, 4, 0.4f, 0.6f) + 1f; //Shift range to 0...2 (instead of -1...1)
            // --HILLS---
            landMap *= 0.6f; //Make less important than mountains

            // ---MOUNTAINS---
            mountainMask = Noise(x, y, z, 4, 0.03f, 0.8f);
            mountainMask = (Clamp(mountainMask, 0.2f, 0.4f) - 0.2f) * 5f; //gradient: 0=land 1=mountains
        
            float mountains = 0f;
            if (mountainMask > 0f)
                mountains = Noise(x, y, z, 6, 0.6f, 0.5f, true) + 1f; //Shift range to 0...2 (instead of -1...1)
            //Apply mountains
            landMap = (landMap * (1 - mountainMask)) + (mountainMask * mountains);
            //Move terrain up by 0.5 to make room for oceans
            landMap = landMap * 0.75f + 0.5f;
        }
        //Apply landMap on continents only
        height += landMask * landMap;

        // ---OCEANS--
        if(landMask < 1)
        {
            float ocean = Noise(x, y, z, 3, 0.5f, 0.9f) + 1f; //Shift range to 0...2 (instead of -1...1)
            ocean *= 0.2f;
            height += (1 - landMask) * ocean;
        }

        //Return biome
        if (height < 0.5f)
            biome = BiomeType.Beach;
        else if (mountainMask > 0.5f)
            biome = BiomeType.Mountains;
        else
        {
            // ---FOREST---
            float forestMask = Noise(x, y, z, 4, 0.07f, 0.8f);
            forestMask = (Clamp(forestMask, 0.2f, 0.4f) - 0.2f) * 5f;
            if (forestMask > 0.5f)
                biome = BiomeType.Forest;
            else biome = BiomeType.Hills; 
        }
        return height;
    }

    float Map(float num, float low, float high)
    {
        //-1...1 -> low...high
        return (num + 1) / 2 * (high - low) + low;
    }

    float Clamp(float num, float low, float high)
    {
        if (num > high) num = high;
        else if (num < low) num = low;
        return num;
    }

    float Noise (float x, float y, float z, int octaves, float frequency, float persistence, bool ridged = false)
    {
        float total = 0f;
        float maxAmplitude = 0f;
        float amplitude = 1f;
        for (int i = 0; i < octaves; i++)
        {
            // Get the noise sample
            if (ridged)
                total += ((1.0f - Mathf.Abs(perlin.GetValue(x * frequency, y * frequency, z * frequency))) * 2.0f - 1.0f) * amplitude;
            else
                total += perlin.GetValue(x * frequency, y * frequency, z * frequency) * amplitude;

            // Make the wavelength twice as small
            frequency *= 2f;

            // Add to our maximum possible amplitude
            maxAmplitude += amplitude;

            // Reduce amplitude according to persistence for the next octave
            amplitude *= persistence;
        }
        return total / maxAmplitude;
    }
}
