﻿using LibNoise;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public enum Sides : int { Top, Bottom, Back, Front, Left, Right }

#pragma warning disable CS0618 //Hide all the UNET deprecation warnings
public class PlanetObject : NetworkBehaviour, ISavable
{
    [SyncVar]
    public string ID = "";
    [SyncVar]
    public int chunkCount = 3;
    [SyncVar]
    public int YChunks = 2;
    //Upper bound of the atmosphere
    [SyncVar]
    public int atmosphereHeight = 200;
    [SyncVar]
    public PlanetType type = PlanetType.Green;

    public Planet data;

    public GameObject atmospherePrefab;
    public GameObject groundPrefab;
    public GameObject chunkPrefab;

    //Needs to be true for network
    public bool generateChunksAtStart = true;
    public bool generateOres = true;
    internal bool[] heightmapInQueue = new bool[6];
    
    //Height where a voxel is in its standard size
    public int standardHeight;
    //Height at which generated heightmap is applied
    public int baseHeight;
    //Maximum height for terrain generation
    public int maxHeight;

    public List<ChunkTemplate> chunks = new List<ChunkTemplate>();

    private TerrainGenerator terrainGen;
    private float noiseScale = 10f;
    private float[][,] heightMaps = new float[6][,];
    public BiomeType[][,] biomeMaps = new BiomeType[6][,];
    public TreeBuilder treeBuilder;
    private int seed = -1;

    private Vector3[] sideRotations = new Vector3[] {
        new Vector3(0, 0, 0),
		new Vector3(180,0,0),
		new Vector3(90,0,0),
        new Vector3(-90,0,0),    
        new Vector3(0, 0,90),
        new Vector3(0, 0,-90)
    };
        
    // Use this for initialization
    void Start () {
        World.planets.Add(this);
    }

    void OnDestroy()
    {
        World.planets.Remove(this);
    }
    
    //Forwarded from PlanetChunks
    public void OnCollisionEnter(Collision col)
    {
        if(!isServer)
            return;
        if(col.relativeVelocity.magnitude < World.minImpactVelocity)
            return;
        col.collider.SendMessage("ProcessCollision", col, SendMessageOptions.DontRequireReceiver);
    }

    //Only after network variables are set
    public override void OnStartClient()
    {
        //Planet ID
        if (string.IsNullOrEmpty(ID))
            ID = GameManager.GetRandomID(8);
        //Planet data from PlanetType
        data = ResourceBank.resources.FindPlanetByType(type);
        data.Init();
        //Init heights
        maxHeight = chunkCount * Planet.chunkSize;
        baseHeight = chunkCount * Planet.chunkSize - (YChunks * Planet.chunkSize);
        //standardHeight = (baseHeight + maxHeight) / 2;
        //HACK: A different value caused the chunks to misalign. MapToSphere is probably wrong.
        standardHeight = maxHeight;
        //Init terrain generator
        if (seed == -1)
            seed = Random.Range(int.MinValue, int.MaxValue);
        terrainGen = new TerrainGenerator(seed, noiseScale);
        //Init tree builder
        treeBuilder = new TreeBuilder(seed);
        //Generate chunks
        if (generateChunksAtStart)
            GenerateChunks(isServer);
        //Create spheres
        CreateSpheres();
    }

    void CreateSpheres()
    {
        GameObject atmosphere = Instantiate(atmospherePrefab);
        atmosphere.transform.position = transform.position;
        atmosphere.transform.localScale = Vector3.one * (baseHeight + atmosphereHeight) * 2;
        atmosphere.GetComponent<Renderer>().material.color = data.atmosphereColor;
        GameObject ground = Instantiate(groundPrefab);
        ground.transform.position = transform.position;
        ground.transform.localScale = Vector3.one * baseHeight * 2;
        ground.GetComponent<Renderer>().material.color = data.groundColor;
    }

    public void GenerateChunks(bool isHost)
    {
        if (isHost)
            Debug.Log("PLANET: Generating Chunks...");
        else
            Debug.Log("PLANET: Initializing planet...");

        int idx = 0;
        //For 6 sides
        for (int i = 0; i < 6; i++)
        {
            //Create empty side object
            var side = new GameObject("PlanetSide" + i);
            side.transform.position = transform.position;
            side.transform.SetParent(transform);

            //Rotate side
            var rot = Quaternion.Euler(sideRotations[i]);
            side.transform.rotation = rot;

            //Populate it with chunks
            for (int x = 0; x < chunkCount; x++)
            {
                for (int y = chunkCount - YChunks; y < chunkCount; y++)
                {
                    for (int z = 0; z < chunkCount; z++)
                    {
                        //Create chunk at correct position
                        var x2 = x * Planet.chunkSize + (Planet.chunkSize / 2f) - (chunkCount * Planet.chunkSize / 2f);
                        var y2 = y * Planet.chunkSize;
                        var z2 = z * Planet.chunkSize + (Planet.chunkSize / 2f) - (chunkCount * Planet.chunkSize / 2f);
                        var v = new Vector3(x2, y2, z2);

                        var template = new ChunkTemplate {
                            numID = idx,
                            ID = this.ID + "_" + idx,
                            isServer = isHost,
                            initialState = isHost ? ChunkState.Unloaded : ChunkState.Hosted,
                            position = transform.position + (rot * v),
                            rotation = rot,
                            parent = side.transform,
                            planet = this,
                            heightData = new PlanetChunk.HeightMapData {
                                x = x, y = z, h = y
                            },
                            side = (Sides)i
                        };
                        template.CalculateCenter();
                        chunks.Add(template);

                        idx++;
                    }
                }
            }
        }
        Debug.Log("Planet " + ID + " generated with " + idx + " chunks");
    }

    void ScheduleGenerateHightMap(Sides side)
    {
        if (heightmapInQueue[(int)side])
            return;

        heightmapInQueue[(int)side] = true;
        ThreadPool.QueueUserWorkItem(GenerateHeightMap, side);
    }

    void GenerateHeightMap(object o)
    {
        var side = (Sides)o;
		int sz = chunkCount * Planet.chunkSize;
		float[,] ret = new float[sz, sz];
        BiomeType[,] bMap = new BiomeType[sz, sz];
		int y = sz / 2;
		for (int x = 0; x < sz; x++) {
			for(int z = 0; z < sz; z++)
			{
                var vect = new Vector3 (x, y, z) - new Vector3(sz/2, 0, sz/2); //Centered
                var vRot = V3ToSide (vect, side);
                var vSph = MapToSphere (vRot / sz * 2f);
                ret[x, z] = terrainGen.GetHeight(vSph.x, vSph.y, vSph.z, out BiomeType bt);
                bMap[x,z] = bt;
			}
		}

        heightMaps[(int)side] = ret;
        biomeMaps[(int)side] = bMap;
        heightmapInQueue[(int)o] = false;
    }

    public float[,] GetHeightMap(Sides side)
    {
        if (heightMaps[(int)side] == null)
            ScheduleGenerateHightMap(side); 
        return heightMaps[(int)side]; //If null, PlanetChunk will call this again next time
    }

    public void LoadObject(Dictionary<string,string> data)
    {
        foreach (string key in data.Keys) {
            switch (key) {
                case "chunkCount":
                    chunkCount = int.Parse(data[key]);
                    break;
                case "YChunks":
                    YChunks = int.Parse(data[key]);
                    break;
                case "atmosphereHeight":
                    atmosphereHeight = int.Parse(data[key]);
                    break;
                case "seed":
                    seed = int.Parse(data[key]);
                    break;
                case "ID":
                    ID = data[key];
                    break;
                case "PlanetType":
                    type = (PlanetType)System.Enum.Parse(typeof(PlanetType), data[key]);
                    break;
            }
        }
        //The data above will be applied in OnStartClient once Networking is up        
        //Chunks and PlanetSides are generated automatically in OnStartClient
    }

    public ObjectData SaveObject()
    {
        //Save basic object data
        ObjectData save = new ObjectData();
        save.transform = new TransformData(transform);
        save.prefab = "Planet";
        save.data = new Dictionary<string, string>();
        save.data.Add("chunkCount", chunkCount.ToString());
        save.data.Add("YChunks", YChunks.ToString());
        save.data.Add("atmosphereHeight", atmosphereHeight.ToString());
        save.data.Add("seed", seed.ToString());
        save.data.Add("ID", ID);
        save.data.Add("PlanetType", type.ToString());

        //Save unsaved chunks (they normally autosave when unloaded)
        foreach (var chunk in chunks)
        {
            if (chunk.instance != null)
                chunk.instance.SaveChunk();
        }
        
        return save;
    }

	Vector3 V3ToSide(Vector3 v, Sides side)
	{
		switch(side)
		{
			case Sides.Top: break;// nothing
			case Sides.Bottom: v = new Vector3(v.x, -v.y, -v.z); break;
			case Sides.Back: v = new Vector3(v.x, -v.z, v.y); break;
			case Sides.Front: v = new Vector3(v.x, v.z, -v.y); break;
			case Sides.Left: v = new Vector3(-v.y, v.x, v.z); break;
			case Sides.Right: v = new Vector3(v.y, -v.x, v.z); break;
		}
		return v;
	}

	Vector3 MapToSphere(Vector3 v)
	{
		float x2 = v.x * v.x;
		float y2 = v.y * v.y;
		float z2 = v.z * v.z;
		Vector3 s;
		s.x = v.x * Mathf.Sqrt(1f - y2 / 2f - z2 / 2f + y2 * z2 / 3f);
		s.y = v.y * Mathf.Sqrt(1f - x2 / 2f - z2 / 2f + x2 * z2 / 3f);
		s.z = v.z * Mathf.Sqrt(1f - x2 / 2f - y2 / 2f + x2 * y2 / 3f);
		return s;
	}
}
