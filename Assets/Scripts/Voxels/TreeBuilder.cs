﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LibNoise;

public class TreeBuilder
{
    readonly IModule3D noise;

    public TreeBuilder(int seed)
    {
        noise = new LibNoise.Primitive.SimplexPerlin(seed, NoiseQuality.Fast);
    }

    public void Build(int x, int y, int z, ref byte[] blocks, PlanetChunk chunk)
    {
        var pos = chunk.BlockToGlobalPos(x, y, z);
        pos *= 0.5f;
        if (noise.GetValue(pos.x, pos.y, pos.z) < 0.8f)
            return;
        if (x < 2 || z < 2 || x > Planet.chunkSize - 3 || z > Planet.chunkSize - 3)
            return;

        var trunkLength = Mathf.Min(5, Planet.chunkSize - (y + 1));
        for (int i = 1; i <= trunkLength; i++)
        {
            if (i > 2 && i < trunkLength)
            {
                var d = trunkLength - i;
                for (int mx = -d; mx <= d; mx++)
                    for (int mz = -d; mz <= d; mz++)
                        blocks[x + mx + Planet.chunkSize * (y + i + Planet.chunkSize * (z + mz))] = 11;
            }
            if (i == trunkLength)
                blocks[x + Planet.chunkSize * (y + i + Planet.chunkSize * z)] = 11;
            else blocks[x + Planet.chunkSize * (y + i + Planet.chunkSize * z)] = 10;
        }
    }
}
