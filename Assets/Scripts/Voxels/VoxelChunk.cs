﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelChunk : MonoBehaviour {
    public float voxelSize = 1.2f;
    //Due to Unity's vertex count limit per mesh this should not be more than 25
    public const byte chunkSize = 25;
    public float scale = 0.5f;
    
    private byte[,,] blocks = new byte[chunkSize, chunkSize, chunkSize];

    private List<Vector3> verts = new List<Vector3>();
    private List<int> tris = new List<int>();
    private List<Vector2> UVs = new List<Vector2>();

    private Mesh mesh;
    private MeshCollider coll;

    public void SetBlocks(byte[,,] _blocks)
    {
        if ((_blocks.GetLength(0) == chunkSize) && (chunkSize == _blocks.GetLength(1)) && (chunkSize == _blocks.GetLength(2)))
        //if(blocks.Length == _blocks.Length)
        {
            blocks = _blocks;
        }
        else Debug.LogError("Cannot set block array of different size than chunkSize");
    }

    public void LoadChunk()
    {
        for (int x = 0; x < chunkSize; x++)
        {
            for (int y = 0; y < chunkSize; y++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    blocks[x, y, z] = 1;
                }
            }
        }
    }

    void RenderCube(int x, int y, int z, byte blockType = 1)
    {
        //Top
        if (GetBlock(x, y + 1, z) == 0)
        {
            verts.Add(MapToSphere(new Vector3(x, y + 1, z + 1)));
            verts.Add(MapToSphere(new Vector3(x + 1, y + 1, z + 1)));
            verts.Add(MapToSphere(new Vector3(x + 1, y + 1, z)));
            verts.Add(MapToSphere(new Vector3(x, y + 1, z)));
        }
        //Front
        if (GetBlock(x, y, z + 1) == 0)
        {
            verts.Add(MapToSphere(new Vector3(x + 1, y, z + 1)));
            verts.Add(MapToSphere(new Vector3(x + 1, y + 1, z + 1)));
            verts.Add(MapToSphere(new Vector3(x, y + 1, z + 1)));
            verts.Add(MapToSphere(new Vector3(x, y, z + 1)));
        }
        //Right
        if (GetBlock(x + 1, y, z) == 0)
        {
            verts.Add(MapToSphere(new Vector3(x + 1, y, z)));
            verts.Add(MapToSphere(new Vector3(x + 1, y + 1, z)));
            verts.Add(MapToSphere(new Vector3(x + 1, y + 1, z + 1)));
            verts.Add(MapToSphere(new Vector3(x + 1, y, z + 1)));
        }
        //Back
        if (GetBlock(x, y, z - 1) == 0)
        {
            verts.Add(MapToSphere(new Vector3(x, y, z)));
            verts.Add(MapToSphere(new Vector3(x, y + 1, z)));
            verts.Add(MapToSphere(new Vector3(x + 1, y + 1, z)));
            verts.Add(MapToSphere(new Vector3(x + 1, y, z)));
        }
        //Left
        if (GetBlock(x - 1, y, z) == 0)
        {
            verts.Add(MapToSphere(new Vector3(x, y, z + 1)));
            verts.Add(MapToSphere(new Vector3(x, y + 1, z + 1)));
            verts.Add(MapToSphere(new Vector3(x, y + 1, z)));
            verts.Add(MapToSphere(new Vector3(x, y, z)));
        }
        //Bottom
        if (GetBlock(x, y - 1, z) == 0)
        {
            verts.Add(MapToSphere(new Vector3(x, y, z)));
            verts.Add(MapToSphere(new Vector3(x + 1, y, z)));
            verts.Add(MapToSphere(new Vector3(x + 1, y, z + 1)));
            verts.Add(MapToSphere(new Vector3(x, y, z + 1)));
        }
    }

    void CalculateTris()
    {
        for (int i = 0; i < verts.Count; i += 4)
        {
            tris.Add(i); //1
            tris.Add(i + 1); //2
            tris.Add(i + 2); //3
            tris.Add(i); //1
            tris.Add(i + 2); //3
            tris.Add(i + 3); //4
        }
    }

    public void RenderChunk()
    {
        for (int x = 0; x < chunkSize; x++)
        {
            for (int y = 0; y < chunkSize; y++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    if (blocks[x, y, z] != 0)
                    {
                        RenderCube(x, y, z, blocks[x, y, z]);
                    }
                }
            }
        }
        CalculateTris();
        UpdateMesh();
    }

    void UpdateMesh()
    {
        if (mesh != null)
            mesh.Clear();
        else {
            mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = mesh;
        }
        mesh.vertices = verts.ToArray();
        //mesh.uv = UVs.ToArray();
        mesh.triangles = tris.ToArray();
        mesh.RecalculateNormals();

        if(coll == null)
            coll = GetComponent<MeshCollider>();

        coll.sharedMesh=null;
        coll.sharedMesh=mesh;

        verts.Clear();
        UVs.Clear();
        tris.Clear();
    }

    public byte GetBlock(int x, int y, int z)
    {
        if ((x >= 0) && (x < chunkSize) && (y >= 0) && (y < chunkSize) && (z >= 0) && (z < chunkSize))
            return blocks[x, y, z];
        else return 0;
    }

    Vector3 MapToSphere(Vector3 v0)
    {
        Vector3 v = v0 * 2f / chunkSize - Vector3.one;
        Vector3 vc = v0 + transform.localPosition; // + offset;
        v = vc.normalized;

        float x2 = v.x * v.x;
        float y2 = v.y * v.y;
        float z2 = v.z * v.z;
        Vector3 s;
        s.x = v.x * Mathf.Sqrt(1f - y2 / 2f - z2 / 2f + y2 * z2 / 3f);
        s.y = v.y * Mathf.Sqrt(1f - x2 / 2f - z2 / 2f + x2 * z2 / 3f);
        s.z = v.z * Mathf.Sqrt(1f - x2 / 2f - y2 / 2f + x2 * y2 / 3f);
        return s.normalized * Mathf.Max(Mathf.Abs(vc.x), Mathf.Abs(vc.y), Mathf.Abs(vc.z)) - transform.localPosition;
    }
}
