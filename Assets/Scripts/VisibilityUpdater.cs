﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class VisibilityUpdater : MonoBehaviour
{
    public float maxDistance = 500f;
    public GameObject chunkPrefab;
    public float updateDelay = 0.1f;
    public int chunkPoolSize = 100;
    float nextUpdate = 0f;
    Queue<PlanetChunk> chunkPool = new Queue<PlanetChunk>();
	
    // Update is called once per frame
    void Update () {
        if (Time.time < nextUpdate)
            return;

        var myPos = transform.position;
        PlanetObject p = World.FindClosestPlanet(myPos);
        if (p == null)
            return;

        //If distance to planet is too long, we don't need to do ANY visibility updates
        float dist = Vector3.Distance(p.transform.position, myPos) - p.maxHeight;
        if (dist > maxDistance)
            return;

        //Possible optimization:  Don't update all chunks in one frame - update in every frame but only a small subsection
        // - It's better for walking but not for flying
        foreach (var chunk in p.chunks)
        {
            dist = Vector3.Distance(chunk.center, myPos);
            if (chunk.instance == null)
            {
                if (dist < maxDistance)
                {
                    PlanetChunk chunkObj;
                    if (chunkPool.Count == 0)
                    {
                        //Instantiate
                        GameObject obj = Instantiate(chunkPrefab, chunk.position, chunk.rotation, chunk.parent);
                        obj.transform.SetParent(chunk.parent);
                        chunkObj = obj.GetComponent<PlanetChunk>();
                    }
                    else
                    {
                        chunkObj = chunkPool.Dequeue();
                        chunkObj.gameObject.SetActive(true);

                        chunkObj.transform.position = chunk.position;
                        chunkObj.transform.rotation = chunk.rotation;
                        chunkObj.transform.SetParent(chunk.parent);
                    }
                    chunkObj.InitFromTemplate(chunk);
                }
            }
            else
            {
                if (dist < maxDistance + 80f)
                {
                    chunk.instance.VisibilityUpdate(dist);
                }
                else
                {
                    if (chunkPool.Count < chunkPoolSize)
                    {
                        //Unload and save
                        chunk.instance.UnloadChunk();
                        chunkPool.Enqueue(chunk.instance);
                        //Disable
                        chunk.instance.gameObject.SetActive(false);
                        chunk.instance = null;
                    }
                    else
                    {
                        Destroy(chunk.instance.gameObject);
                        chunk.instance = null;
                    }
                }
            }
        }
        nextUpdate = Time.time + updateDelay;
    }
}
