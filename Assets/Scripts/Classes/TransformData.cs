﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable()]
public class TransformData {

    //Position vector
    public float p_x, p_y, p_z;
    //Rotation vector (euler)
    public float r_x, r_y, r_z;


    public Vector3 position {
        get { return new Vector3(p_x, p_y, p_z); }
        set { p_x = value.x; p_y = value.y; p_z = value.z; } 
    }

    public Quaternion rotation {
        get { return Quaternion.Euler(r_x, r_y, r_z); }
        set { var rot = value.eulerAngles;
              r_x = rot.x; r_y = rot.y; r_z = rot.z; }
    }

    public TransformData (Transform transform)
    {
        position = transform.position;
        rotation = transform.rotation;
    }

    public TransformData (byte[] bytes)
    {
        if (bytes.Length < 24)
            throw new ArgumentException("Input bytes array needs to be at least 24 bytes long");
        p_x = BitConverter.ToSingle(bytes, 0);
        p_y = BitConverter.ToSingle(bytes, 4);
        p_z = BitConverter.ToSingle(bytes, 8);
        r_x = BitConverter.ToSingle(bytes, 12);
        r_y = BitConverter.ToSingle(bytes, 16);
        r_z = BitConverter.ToSingle(bytes, 20);
    }

    public byte[] GetBytes()
    {
        List<byte> bytes = new List<byte>();
        bytes.AddRange(BitConverter.GetBytes(p_x));
        bytes.AddRange(BitConverter.GetBytes(p_y));
        bytes.AddRange(BitConverter.GetBytes(p_z));
        bytes.AddRange(BitConverter.GetBytes(r_x));
        bytes.AddRange(BitConverter.GetBytes(r_y));
        bytes.AddRange(BitConverter.GetBytes(r_z));
        return bytes.ToArray();
    }

    public byte[] Serialize()
    {
        BinaryFormatter bf = new BinaryFormatter();  
        var ms = new System.IO.MemoryStream();  
        bf.Serialize(ms, this);

        return ms.ToArray();
    }
}
