﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WorldBuilder : MonoBehaviour
{
    public int seed;
    public GameObject planetPrefab;

    public int planetCount = 3;
    public int minDistance = 4000;
    public int maxDistance = 10000;
    public int maxOffset = 5000;
    public int maxOffsetY = 1000;
    public int minChunks = 30;
    public int maxChunks = 100;

    bool worldBuilt = false;

    public void Build()
    {
        //This shouldn't happen
        if (worldBuilt)
        {
            Debug.LogWarning("World already built. Nothing to do.");
            return;
        }

        //First planet is at constant position to be easily reachable by the player.
        int distance = 0;
        int offset = 0;
        int offsetY = 0;

        var rng = new System.Random(seed);
        
        //TODO: Show progress
        for (int i = 0; i < planetCount; i++)
        {
            Debug.Log("[WB] Generating planet " + (i+1) + "/" + planetCount);
            //Choose random planet type
            int numTypes = System.Enum.GetNames(typeof(PlanetType)).Length;
            var type = (PlanetType)rng.Next(numTypes);
            //Random planet size (dependent on type?)
            int chunkCount = rng.Next(minChunks, maxChunks);
            if (i == 0)
                distance = chunkCount * Planet.chunkSize + ResourceBank.resources.FindPlanetByType(type).atmosphereHeight;
            var position = new Vector3(offset, offsetY, distance);
            //Create planet
            CreatePlanet(position, type, chunkCount);

            //Move to next orbit
            distance += rng.Next(minDistance, maxDistance);
            //Have the planet offset a bit so they are not all in line
            offset = rng.Next(-maxOffset, maxOffset);
            offsetY = rng.Next(-maxOffsetY, maxOffsetY);
        }

        worldBuilt = true;
    }

    void CreatePlanet(Vector3 pos, PlanetType type, int chunkCount)
    {
        GameObject obj = Instantiate(planetPrefab, pos, Quaternion.identity);
        var planet = obj.GetComponent<PlanetObject>();
        planet.type = type;
        planet.chunkCount = chunkCount;
        //We should generate chunks now, not at start.
        //planet.generateChunksAtStart = false;
        //-> First the planet needs to be initialized
        //planet.GenerateChunks(true);

        NetworkServer.Spawn(obj);
    }

}
