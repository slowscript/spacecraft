﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Biome
{
    public TerrainLayer[] layers;
    public TreeType[] treeTypes;
    public int[] grassTypes;
    public GameObject[] surfaceStructures;
}

public class TerrainLayer
{
    public int minDepth;
    public byte voxel;
}

public enum TreeType : byte { Generic }

public enum BiomeType : byte { Hills, Beach, Mountains, Forest };

public static class Biomes
{
    //Indexes must correspond to BiomeType
    public static readonly Biome[] biomeList = new Biome[]
    { 
        new Biome { //Hills
            layers = new TerrainLayer[] {
                new TerrainLayer { minDepth = 5, voxel = 1 }, //Stone
                new TerrainLayer { minDepth = 2, voxel = 2 }, //Dirt
                new TerrainLayer { minDepth = 0, voxel = 3 }  //Grass
            }
        },
        new Biome { //Beach
            layers = new TerrainLayer[] {
                new TerrainLayer { minDepth = 5, voxel = 1 }, //Stone
                new TerrainLayer { minDepth = 0, voxel = 9 }  //Sand
            }
        },
        new Biome { //Mountains
            layers = new TerrainLayer[] {
                new TerrainLayer { minDepth = 0, voxel = 1 }, //Stone
            }
        },
        new Biome { //Forest
            layers = new TerrainLayer[] {
                new TerrainLayer { minDepth = 5, voxel = 1 }, //Stone
                new TerrainLayer { minDepth = 0, voxel = 2 }  //Dirt
            },
            treeTypes = new TreeType[] { TreeType.Generic }
        }
    };
}
