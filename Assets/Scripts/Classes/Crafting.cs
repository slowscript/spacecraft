﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CraftingTool
{
    //TODO: Furnace should be separate
    Hand, Furnace, Anvil, Workbench
}

public struct CraftingRecipe
{
    public CraftingTool tool;
    public string materials;
    public int product;
    public float time;

    public CraftingRecipe(CraftingTool _tool, string _materials, int _product, float _time = 0f)
    {
        tool = _tool;
        materials = _materials;
        product = _product;
        time = _time;
    }
}

public class Crafting
{
    public static CraftingRecipe[] recipes = {
        //Add new items to ResourceBank!!!
        new CraftingRecipe(CraftingTool.Hand, "9:10x1", 22),           //Wood->Wooden board (multiple?)
        new CraftingRecipe(CraftingTool.Hand, "22x6", 10),             //Workbench
        new CraftingRecipe(CraftingTool.Hand, "9:1x3", 11),            //Furnace
        new CraftingRecipe(CraftingTool.Hand, "9:1x5", 23),            //Stone anvil
        new CraftingRecipe(CraftingTool.Hand, "24x1;25x1", 26),        //Improvised multitool
        new CraftingRecipe(CraftingTool.Furnace, "9:4x1", 12, 5f),     //Iron -> ingot
        new CraftingRecipe(CraftingTool.Anvil, "12x1", 13),            //Iron -> rod
        new CraftingRecipe(CraftingTool.Anvil, "12x1", 14),            //Iron -> plate
        new CraftingRecipe(CraftingTool.Anvil, "12x2;24x1", 28),       //Iron pickaxe
        new CraftingRecipe(CraftingTool.Anvil, "12x1;24x1", 2),        //Hammer
        new CraftingRecipe(CraftingTool.Furnace, "9:5x1", 15, 5f),     //Gold -> ingot
        new CraftingRecipe(CraftingTool.Furnace, "9:12x1", 31, 5f),    //Copper -> ingot
        new CraftingRecipe(CraftingTool.Anvil, "31x1", 32),            //Copper -> wire
        new CraftingRecipe(CraftingTool.Workbench, "32x1", 33),        //Copper coil
        new CraftingRecipe(CraftingTool.Furnace, "9:9x1", 27, 7f),     //Sand -> glass pane
        new CraftingRecipe(CraftingTool.Workbench, "14x6;13x4", 1),    //Cube
        new CraftingRecipe(CraftingTool.Workbench, "14x4;13x2", 35),   //Slope
        new CraftingRecipe(CraftingTool.Workbench, "14x4;13x3", 36),   //Slope corner inner
        new CraftingRecipe(CraftingTool.Workbench, "14x3;13x1", 37),   //Slope corner outer
        new CraftingRecipe(CraftingTool.Workbench, "14x6;13x4", 17),   //Small Container
        new CraftingRecipe(CraftingTool.Workbench, "13x4;27x1", 7),    //Window
        new CraftingRecipe(CraftingTool.Workbench, "14x4;13x2", 8),    //Door
        new CraftingRecipe(CraftingTool.Workbench, "14x2;13x3", 38),   //Welding frame
        new CraftingRecipe(CraftingTool.Workbench, "11x1;13x8;14x16", 29),//Steam engine
        new CraftingRecipe(CraftingTool.Workbench, "33x6;32x4;13x6;14x6", 30),//Electric motor
        new CraftingRecipe(CraftingTool.Workbench, "14x4;15x2;16x1", 21) //Gravity generator -- temporary
    };

    public static CraftingRecipe FindRecipeForItem(Item item, CraftingTool tool)
    {
        foreach (var r in Crafting.recipes)
        {
            if(r.tool == tool)
            {
                var material = Item.Deserialize(r.materials);
                if (material.Matches(item))
                {
                    return r;
                }
            }
        }
        return new CraftingRecipe();
    }
}
