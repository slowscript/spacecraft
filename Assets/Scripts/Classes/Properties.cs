﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Properties : MonoBehaviour
{
    public delegate void PropertyChanged(BlockProp prop);
    public event PropertyChanged OnPropertyChanged;
    List<BlockProp> props = new List<BlockProp>();

    public List<BlockProp> GetProperties()
    {
        return props;
    }

	public void Add(BlockProp prop)
	{
		props.Add(prop);
	}

    public Dictionary<string, string> Save()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        foreach(var prop in props)
        {
            dict.Add(prop.name, prop.value);
        }
        return dict;
    }

    public void Load(Dictionary<string,string> d)
    {
        foreach(var name in d.Keys)
        {
            if(!SetProperty(name, d[name]))
                Debug.LogWarning("Invalid property: " + name);
        }
    }

    public bool SetProperty(string n, string val)
    {
        BlockProp p = props.Find((x) => x.name == n);
		if(p.name != n)
			return false;
		
        if(p.type == PropType.Int)
        {
            if(int.Parse(val) > p.maxValue)
                return false;
        }
        p.value = val;
        if(OnPropertyChanged != null)
            OnPropertyChanged(p);
        return true;
    }

    public string GetString(string n)
    {
        var p = props.Find((x) => x.name == n);
        return p.value;
    }
    public int GetInt(string n)
    {
        var p = props.Find((x) => x.name == n);
        return int.Parse(p.value);
    }

    public bool GetBool(string n)
    {
        var p = props.Find((x) => x.name == n);
        return bool.Parse(p.value);
    }
}

public enum PropType
{
    Bool, String, Int
}

public class BlockProp
{
    public string name;
    public PropType type;
    public string value;
    public int maxValue;

    public BlockProp(string _n, PropType _t, string _v, int _max = int.MaxValue)
    {
        name = _n;
        type = _t;
        value = _v;
        maxValue = _max;
    }
}