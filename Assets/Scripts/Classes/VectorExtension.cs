﻿using UnityEngine;

public static class VectorExtension
{
    public static Vector3 Multiply(this Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
    }

    public static Vector3 Round(this Vector3 v)
    {
        return new Vector3(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
    }

    public static Vector3 Mask(this Vector3 v, Vector3 mask, Vector3 newVect)
    {
        if (mask.x != 0f) v.x = newVect.x;
        if (mask.y != 0f) v.y = newVect.y;
        if (mask.z != 0f) v.z = newVect.z;
        return v;
    }

    public static Vector3 Mask2(this Vector3 v, Vector3 mask, Vector3 newVect)
    {
        if (mask.x == 0f) v.x = newVect.x;
        if (mask.y == 0f) v.y = newVect.y;
        if (mask.z == 0f) v.z = newVect.z; 
        return v;
    }

    public static Vector3 RotateAroundPivot(this Vector3 point, Vector3 pivot, Vector3 angles)
    {
        Vector3 dir  = point - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(angles) * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point; // return it
    }

    public static Vector3 Abs(this Vector3 v)
    {
        var newV = new Vector3();
        newV.x = Mathf.Abs(v.x);
        newV.x = Mathf.Abs(v.x);
        newV.x = Mathf.Abs(v.x);
        return newV;
    }

    public static int VectorToSide(this Vector3 v)
    {
        for(int i = 0; i < MountPoints.directions.Length; i++)
        {
            if (v.IsCloseAngle(MountPoints.directions[i]))
                return i;
        }
        //Fallback, expected to return -1
        return System.Array.IndexOf(MountPoints.directions, v);
    }

    public static bool IsCloseAngle(this Vector3 v, Vector3 v2, float delta = 1f)
    {
        return Vector3.Angle(v, v2) < delta;
    }
    public static bool IsClose(this Vector3 v, Vector3 v2, float delta = 0.05f)
    {
        return (v - v2).magnitude < delta;
    }
    public static Vector3 xzy(this Vector3 v)
    {
        return new Vector3(v.x, v.z, v.y);
    }

    public static Vector3 yzx(this Vector3 v)
    {
        return new Vector3(v.y, v.z, v.x);
    }

    public static Vector3 zxy(this Vector3 v)
    {
        return new Vector3(v.z, v.y, v.y);
    }

    public static Vector2 Add(this Vector2 v, float n)
    {
        return new Vector2(v.x + n, v.y + n);
    }

    public static Vector2 Sqrt(this Vector2 v)
    {
        return new Vector2(Mathf.Sqrt(v.x), Mathf.Sqrt(v.y));
    }

    public static Vector3 Sign(this Vector3 v)
    {
        return new Vector3(Mathf.Sign(v.x), Mathf.Sign(v.y), Mathf.Sign(v.z));
    }

    public static Vector3 Floor(this Vector3 v)
    {
        return new Vector3(Mathf.Floor(v.x), Mathf.Floor(v.y), Mathf.Floor(v.z));
    }

    //Scale = 1 / gridSize
    public static Vector3 SnapToGrid(this Vector3 v, float scale)
    {
        v *= scale;
        v = v.Round();
        v /= scale;
        return v;
    }
}

