﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISavable {

    ObjectData SaveObject();
    void LoadObject(Dictionary<string,string> data);

}
