﻿using LibNoise;
using UnityEngine;

[System.Serializable]
public class Ore
{
    public string name = "Ore";
    public byte block = 3;
    public float treshold = 0.9f;

    private const float scale = 0.05f;

    IModule3D noise;

    public Ore(byte _block, float _treshold, bool _noise = false)
    {
        block = _block;
        name = ResourceBank.voxelNames[block];
        treshold = _treshold;

        if (_noise)
            InitNoise();
    }

    public void InitNoise()
    {
        noise = new LibNoise.Primitive.SimplexPerlin(Random.Range(int.MinValue, int.MaxValue), NoiseQuality.Fast);
    }

    public bool GenerateAt(float x, float y, float z)
    {
        var val = noise.GetValue(x * scale, y * scale, z * scale);
        return val > treshold; 
    }
}
