﻿public interface IElectricAppliance
{
    void OnPoweredOn(); //Tell the appliance it should start doing its job, because all conditions are met

    void OnPoweredOff(); //Tell the appliance to stop doing its job
}
