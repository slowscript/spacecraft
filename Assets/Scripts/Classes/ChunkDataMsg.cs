﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEngine.Networking;

public class ChunkDataMsg : MessageBase
{
    public const int id = MsgType.Highest + 7;
    public string target;
    public byte[] data;

    public static ChunkDataMsg Create(byte[] _data, string _target)
    {
        using (var msOut = new MemoryStream())
        {
            using (var gz = new GZipStream(msOut, CompressionMode.Compress, true))
            {
                using (var msIn = new MemoryStream(_data))
                {
                    msIn.CopyTo(gz);

                    var msg = new ChunkDataMsg();
                    msg.data = msOut.ToArray();
                    msg.target = _target;
                    return msg;
                }
            }
        }
    }

    public byte[] GetData()
    {
        using (var msIn = new MemoryStream(data))
        {
            using (var gz = new GZipStream(msIn, CompressionMode.Decompress))
            {
                using (var msOut = new MemoryStream())
                {
                    gz.CopyTo(msOut);
                    return msOut.ToArray();
                }
            }
        }
    }
}
