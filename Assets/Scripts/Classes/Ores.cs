﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ores {

    //0 = air
    //1 = stone
    //2 = dirt
    //3 = grass
    public static readonly Ore iron = new Ore(4, 0.9f); //Iron
    public static readonly Ore gold = new Ore(5, 0.95f); //Gold
    public static readonly Ore diamond = new Ore(6, 0.98f); //Diamond
    //8 = bedrock
    public static readonly Ore coal = new Ore(8, 0.8f); //Coal
}
