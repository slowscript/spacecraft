﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlanetType
{
    Green, Desert, Hot
}

[System.Serializable()]
public class Planet
{
    public const byte chunkSize = 24; //<-- Due to Unity's vertex count limit per mesh this should not be more than 25
    public const float tileSize = 0.25f;
    public static readonly Vector2[] tilePos = new Vector2[] {
        new Vector2(0, 0), new Vector2(0, 0), new Vector2(0.25f, 0), new Vector2(0.5f, 0),
        new Vector2(0.25f, 0.5f), new Vector2(0.75f, 0.75f), new Vector2(0.25f, 0.75f), new Vector2(0f, 0.25f),
        new Vector2(0, 0.75f), new Vector2(0.25f, 0.25f), new Vector2(0.5f, 0.25f), new Vector2(0.75f, 0.25f),
        new Vector2(0.5f, 0.5f)
    };

    public PlanetType type;
    //Ores to be found on planet
    public Ore[] ores;
    //Block height of sea level
    public int seaLevel;

    //Upper bound of the atmosphere
    public int atmosphereHeight;
    //TODO: Use a prefab to achieve better atmosphere
    public Color atmosphereColor;
    public Color groundColor;

    public void Init()
    {
        foreach(var ore in ores)
        {
            ore.InitNoise();
        }
    }

    public static Sprite GetTile(int i)
    {
        var tex = ResourceBank.resources.tileTexture;
        var r = new Rect(tilePos[i] * tex.width, Vector2.one * tileSize * tex.width);
        return Sprite.Create(tex, r, Vector2.one * 0.5f);
    }
}
