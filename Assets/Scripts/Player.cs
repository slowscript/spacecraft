﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

#pragma warning disable CS0618 //Hide all the UNET deprecation warnings
public class Player : NetworkBehaviour, ISavable
{
    public static Player local;

    [SyncVar]
    [SerializeField]
    float _health = 100f;
    public float Health {
        get { return _health; }
        private set { _health = value; }
    }
    public float impactDamageFactor = 0.05f; //Can be changed with armor etc...
    public PlayerControler controller;
    public PlayerInput input;
    public Transform cam;
    internal ToolBar toolbar;
    internal InventoryUI inventory;
    internal HUD hud;

    void Awake()
    {
        hud = FindObjectOfType<HUD>();
        toolbar = FindObjectOfType<ToolBar>();
        inventory = hud.inventory;
    }

    void Start()
    {
        if (isLocalPlayer)
            local = this;

        transform.position = GameManager.playerPos;
        transform.rotation = GameManager.playerRot;
        LoadObject(GameManager.playerData);
    }    

    public bool PickupItem(Item itm)
    {
        if (toolbar.inventory.AddItem(itm))
        {
            toolbar.UpdateUI();
            return true;
        }
        else //Store the rest in inventory
        {
            return inventory.inventory.AddItem(itm);
        }
    }

    public void DamagePlayer(float damage)
    {
        Health -= damage;
        print("Player damaged: -" + damage + " new: " + Health);
        if (Health <= 0f)
            Die();
    }

    void Die()
    {
        hud.DisplayMessage("You should be dead by now. I just need to implement the death screen...");
    }

    void ProcessCollision(Collision c)
    {
        var damage = c.impulse.magnitude / Time.fixedDeltaTime * impactDamageFactor;
        if (damage > 5f)
            DamagePlayer(damage);
    }

    public ObjectData SaveObject()
    {
        ObjectData data = new ObjectData();
        data.transform = new TransformData(transform);
        data.prefab = "Player";
        data.data = new Dictionary<string, string>();
        data.data.Add("inventory", inventory.inventory.Serialize());
        data.data.Add("toolbar", toolbar.inventory.Serialize());
        return data;
    }

    public void LoadObject(Dictionary<string,string> data)
    {
        foreach (string key in data.Keys) {
            switch (key) {
                case "inventory":
                    inventory.inventory.Deserialize(data[key]);
                    inventory.UpdateUI();
                    break;
                case "toolbar":
                    toolbar.inventory.Deserialize(data[key]);
                    toolbar.UpdateUI();
                    break;
            }
        }
    }
}
