﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkChild : NetworkBehaviour
{
    [SyncVar(hook="OnStartClient")]
    public NetworkInstanceId parentNetID;

    public override void OnStartClient()
    {
        var parent = ClientScene.FindLocalObject(parentNetID);
        if (parent != null)
            transform.SetParent(parent.transform);
        else
            Debug.LogWarning(transform.name + " became an orphan on this client");
    }
}
