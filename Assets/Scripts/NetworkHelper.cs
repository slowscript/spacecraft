﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

#pragma warning disable CS0618 //Hide all the UNET deprecation warnings
public class NetworkHelper : NetworkBehaviour {

    public static NetworkHelper current;

    public Transform cam;
    private ResourceBank bank;

    void Start()
    {
        if (isServer)
            bank = GameObject.FindGameObjectWithTag("GM").GetComponent<ResourceBank>();
        if (isLocalPlayer && (current == null))
            current = this;
    }

    [Command]
    public void CmdPlaceBlock(int block, Vector3 mp, Vector3 md, float tw)
    {
        RaycastHit hit;
        Vector3 newPos;
        Quaternion newRot = cam.rotation;
        bool hitSuccess = false;
        if (Physics.Raycast(cam.position, cam.forward, out hit, Tool.maxActivateDst))
        {
            hitSuccess = true;
            newPos = PlaceBlockTool.CalculateBlockPos(hit, out newRot, bank.items[block].prefab, cam, mp, md, tw);
        }
        else newPos = PlaceBlockTool.CalculateBlockPos(cam);

        //If we hit a buildable block we hit a ship, otherwise we have to create a new ship
        if (hitSuccess && (hit.transform.tag == "Buildable"))
        {
            var parent = hit.transform.gameObject;
            SpawnObjectAsChild(block, parent, newPos, newRot);
            parent.GetComponent<Ship>().RpcUpdateShip();

        }
        else
            SpawnObjectAndChild(0, block, newPos, newRot);
    }

    [Command]
    public void CmdDestroyBlock(GameObject obj)
    {
        obj.GetComponent<Block>().CmdDelete();
    }

    [Command]
    public void CmdDestroyObject(GameObject obj)
    {
        NetworkServer.Destroy(obj);
    }

    [Command]
    public void CmdDetachBlock(GameObject obj)
    {
        var newShip = Instantiate(bank.ship, obj.transform.position, obj.transform.rotation);
        obj.transform.SetParent(newShip.transform);
        NetworkServer.Spawn(newShip);
        SetNetworkParent(obj, newShip);
    }

    public void SpawnObjectAndChild(int parent, int child, Vector3 pos, Quaternion rot)
    {
        GameObject newParent = Instantiate(bank.items[parent].prefab, pos, rot);
        GameObject newChild = Instantiate(bank.items[child].prefab, pos, rot);
        newChild.name = bank.items[child].prefab.name;
        newChild.transform.SetParent(newParent.transform);
        NetworkServer.Spawn(newParent);
        SetNetworkParent(newChild, newParent);
        NetworkServer.Spawn(newChild);
        RpcSyncLocalTransform(newChild, newChild.transform.localPosition, newChild.transform.localRotation);
    }

    public void SpawnObjectAsChild(int child, GameObject parent, Vector3 pos, Quaternion rot)
    {
        GameObject block = GameObject.Instantiate (bank.items[child].prefab, pos, rot);
        block.name = bank.items[child].prefab.name;
        block.transform.SetParent(parent.transform);
        SetNetworkParent(block, parent);
        NetworkServer.Spawn(block);
        RpcSyncLocalTransform(block, block.transform.localPosition, block.transform.localRotation);
    }
        
    [Command]
    public void CmdCreatePlanet()
    {
        GameObject obj = Instantiate(bank.planet, new Vector3(0, 0, 2000), Quaternion.identity);
        obj.GetComponent<PlanetObject>().generateChunksAtStart = true;
        NetworkServer.Spawn(obj);
    }

    [Command]
    public void CmdRequestData(int connID, string chunkID)
    {
        var chunk = World.FindChunkWithID(chunkID);
        chunk.RequestData(connID);
    }

    [Command]
    public void CmdUpdateVoxel(string chunkID, int x, int y, int z, byte val)
    {
        RpcUpdateVoxel(chunkID, x, y, z, val);
    }

    [ClientRpc]
    public void RpcUpdateVoxel(string chunkID, int x, int y, int z, byte val)
    {
        var chunk = World.FindChunkWithID(chunkID);
        if (chunk.chunkState == ChunkState.Loaded)
        {
            chunk.UpdateBlock(x, y, z, val);
            chunk.RebuildMesh();
        }
    }

    [ClientRpc]
    public void RpcSetObjectParent(GameObject block, GameObject parent)
    {
        block.transform.SetParent(parent.transform);
    }

    [ClientRpc]
    public void RpcSyncLocalTransform(GameObject obj, Vector3 pos, Quaternion rot)
    {
        obj.transform.localPosition = pos;
        obj.transform.localRotation = rot;
    }

    [ClientRpc]
    public void RpcDetachChildren(GameObject obj)
    {
        obj.transform.DetachChildren();
    }

    public static void SetNetworkParent(GameObject child, GameObject parent)
    {
        try {
            child.GetComponent<NetworkChild>().parentNetID = parent.GetComponent<NetworkIdentity>().netId;
        } catch {
            Debug.LogError("Couldn't set network parent. Do passed GameObjects have required components?");
        }
    }
}
